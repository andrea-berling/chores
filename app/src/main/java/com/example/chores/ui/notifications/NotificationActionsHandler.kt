package com.example.chores.ui.notifications

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import com.example.chores.MainApplication
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.delay_task.DelayTaskActivity
import com.example.chores.util.Constants.ACTION_DELAY_TASK
import com.example.chores.util.Constants.ACTION_MARK_ONGOING
import kotlinx.coroutines.runBlocking
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SetStatusAsyncTask(private val pendingResult: BroadcastReceiver.PendingResult) : AsyncTask<Any,Void, Void>() {

    override fun doInBackground(vararg args: Any?): Void? {
        runBlocking {
            val taskRepository : TaskRepository = args[2] as TaskRepository
            taskRepository.setTaskStatus(args[0] as String,args[1] as TaskStatus)
        }

        return null
    }

    override fun onPostExecute(result: Void?) {
       pendingResult.finish()
    }
}

class NotificationActionsHandler : BroadcastReceiver(), KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())

    private val taskRepository : TaskRepository by instance()

    override fun onReceive(p0: Context?, intent: Intent?) {
        val action = intent?.action
        action?.run {
            val notificationId = intent.getIntExtra("notificationId",0)
            (MainApplication.applicationContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).cancel(notificationId)
            when(this) {
                ACTION_MARK_ONGOING -> markOngoing(intent)
                ACTION_DELAY_TASK -> delayTask(intent)
                else -> {}
            }
        }
    }

    private fun delayTask(intent: Intent?) {
        val task = intent?.extras?.getBundle("taskBundle")?.getParcelable<Task>("task")
        task?.run {
            val delayTaskIntent = Intent(MainApplication.applicationContext(),DelayTaskActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                putExtra("task",this@run)
            }
            MainApplication.applicationContext().startActivity(delayTaskIntent)
        }
    }

    private fun markOngoing(intent: Intent?) {
        val task = intent?.extras?.getBundle("taskBundle")?.getParcelable<Task>("task")
        task?.run {
            val pendingResult = goAsync()
            SetStatusAsyncTask(pendingResult).execute(task.id,TaskStatus.ONGOING, taskRepository)
        }
    }

}