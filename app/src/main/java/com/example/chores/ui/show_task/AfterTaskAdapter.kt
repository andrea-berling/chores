package layout

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chores.R
import com.example.chores.data.model.TaskList
import com.example.chores.data.model.TaskStatus

class AfterTaskAdapter(var afterTasks: TaskList) : RecyclerView.Adapter<AfterTaskAdapter.ViewHolder>() {

    var listener : OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(v: View, position: Int)
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class ViewHolder(itemView: View, private val listener: OnItemClickListener?) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
        override fun onClick(v: View?) {
            listener?.run {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onItemClick(itemView, position)
            }
        }

        var title : TextView = itemView.findViewById(R.id.task_title)
        var checkbox : CheckBox = itemView.findViewById(R.id.checkbox)

        init {
            itemView.setOnClickListener(this)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.after_task_row_layout,parent,false)

        return ViewHolder(view,listener).also {
            view.setOnClickListener(it)
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.title.text = afterTasks[position].name
        holder.checkbox.isChecked = afterTasks[position].status == TaskStatus.COMPLETED
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = afterTasks.size
}
