package com.example.chores.ui.delay_task

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chores.data.model.NotificationUnit
import com.example.chores.data.model.Task
import com.example.chores.data.repository.TaskRepository
import kotlinx.coroutines.launch
import java.util.*

class DelayTaskViewModel(private val taskRepository: TaskRepository) : ViewModel() {

    fun updateTaskTime() {
        task.datetime = date.value!!
        // !! is justified as it was initialized in onCreate
        task.notificationTime = notificationTime.value!!
        task.notificationUnit = notificationUnit.value!!
        viewModelScope.launch {
            taskRepository.updateTask(task)
        }
    }

    var date = MutableLiveData<Calendar>()
    var notificationTime = MutableLiveData<Int>()
    var notificationUnit = MutableLiveData<NotificationUnit>()
    lateinit var task : Task
}