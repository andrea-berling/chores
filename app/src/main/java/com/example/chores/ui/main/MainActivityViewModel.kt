package com.example.chores.ui.main

import androidx.lifecycle.*
import com.example.chores.MainApplication
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskClass
import com.example.chores.data.model.TaskList
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.settings.SettingsValidator
import kotlinx.coroutines.launch
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivityViewModel(private val taskRepository: TaskRepository) : ViewModel(),KodeinAware {

    override val kodein: Kodein by kodein(MainApplication.applicationContext())

    private val settingsValidator : SettingsValidator by instance()
    private var tasksOldSource : LiveData<TaskList>? = null
    private var tasksLiveData = MediatorLiveData<TaskList>()
    var tasks = TaskList()
    lateinit var classes : LiveData<List<TaskClass>>
    var classIndex = -1
    var toolBarTitle = MutableLiveData<String>()

    var editTaskClassId = ""
    var editTaskClassName = MutableLiveData<String>()
    var editTaskClassPriority = MutableLiveData<TaskPriority>()

    fun getClassesLiveData() : LiveData<List<TaskClass>> {
        classes = taskRepository.retrieveClassesLiveData()
        return classes
    }

    suspend fun getClass(classId: String): TaskClass = taskRepository.retrieveClass(classId)!!

    fun createTask(task: Task) {
        viewModelScope.launch {
            taskRepository.addTask(task)
        }
    }

    private fun updateTasksWith(newSource : LiveData<TaskList>) {
        tasksOldSource?.run {
            tasksLiveData.removeSource(this)
        }

        tasksLiveData.addSource(newSource) {
            tasksLiveData.value = it.filter { task ->
                return@filter settingsValidator.shouldShow(task)
            }
        }

        tasksOldSource = newSource
    }

    private fun getTasksForClass(classId: String) : LiveData<TaskList> {
        // !! is justified as the classId is always set to an existing class' id
        updateTasksWith(taskRepository.retrieveTasksOfClassLiveData(classId)!!)
        return tasksLiveData
    }

    fun getAllTasksLiveData(): LiveData<TaskList> {
        classIndex = -1
        updateTasksWith(taskRepository.retrieveAllTasksLiveData())
        return tasksLiveData
    }

    fun getClassWithIndexTasksLiveData(index: Int) {
        classes.value?.run {
            classIndex = index
            getTasksForClass(classes.value!![index].id)
        }
    }

    suspend fun deleteCurrentClass() {
        taskRepository.removeClass(classes.value!![classIndex].id)
    }

    suspend fun retrieveTaskWithEverything(taskId: String) : Task {
        return taskRepository.retrieveTaskWithEverything(taskId)!!
    }

    fun updateTasks() {
        when {
            classIndex >= 0 -> getClassWithIndexTasksLiveData(classIndex)
            classIndex == -1 -> getAllTasksLiveData()
            classIndex == -2 -> getFutureTasksLiveData()
        }
    }

    fun getTasksLiveData(): LiveData<TaskList> {
        return tasksLiveData
    }

    fun getFutureTasksLiveData(): LiveData<TaskList> {
        classIndex = -2
        updateTasksWith(taskRepository.retrieveFutureTasksLiveData(null))
        return tasksLiveData
    }

    fun addClass(taskClass: TaskClass, newClass: Boolean) {
        viewModelScope.launch {
            if (newClass)
                taskRepository.addClass(taskClass.name,taskClass.priority)
            else
                taskRepository.updateClass(taskClass)
        }
    }

}