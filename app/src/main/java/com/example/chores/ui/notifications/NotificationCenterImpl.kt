package com.example.chores.ui.notifications

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import com.example.chores.MainApplication
import com.example.chores.data.model.Task

class NotificationCenterImpl: NotificationCenter {

    private fun getNotificationIntentForTask( context: Context, task: Task) : Intent {
        return Intent(context, NotificationPoster::class.java).apply {
            /* This is totally unnecessary code that wraps the task with a Bundle
            Apparently Android >= N has problems unmarshalling Parcelable in the extra of an intent
            with the AlarmService, and thus loses the task when trying to retrieve it from the intent
            that started the NotificationPoster service.

            Everything works fine without the Bundle in Android 5.1.1 Lollipop, but doesn't in Android 9 Pie.

            Once again, Android sucks
             */
            val b = Bundle().apply {
                putParcelable("task", task)
            }
            putExtra("taskBundle", b)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                identifier = task.id
            else
                data = Uri.parse("chores://notifications/${task.id}")
        }
    }

    private fun getAlarmServicePendingIntent(task : Task) : PendingIntent {
        val context = MainApplication.applicationContext()

        val intent = getNotificationIntentForTask(context, task)

        return PendingIntent.getBroadcast(context, 0,intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }


    override fun addNotification(task : Task): Boolean {

        return if (!isNotificationSet(task)) {
            val alarmTime = task.datetime.timeInMillis - task.getNotificationOffsetInMillis()
            val context = MainApplication.applicationContext()
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntent = getAlarmServicePendingIntent(task)

            alarmManager.setExact(AlarmManager.RTC_WAKEUP,alarmTime,pendingIntent)

            true
        } else
            false
    }

    override fun removeNotification(task : Task): Boolean {

        return if (isNotificationSet(task)) {
            val context = MainApplication.applicationContext()
            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val pendingIntent = getAlarmServicePendingIntent(task)

            alarmManager.cancel(pendingIntent)
            pendingIntent.cancel()

            true
        } else
            false

    }

    override fun updateNotification(task: Task): Boolean {
        return if(removeNotification(task))
            addNotification(task)
        else
            false
    }

    override fun isNotificationSet(task: Task): Boolean {

        val context = MainApplication.applicationContext()

        val intent = getNotificationIntentForTask(context, task)

        val pendingIntent : PendingIntent? = PendingIntent.getBroadcast(context, 0,intent,
            PendingIntent.FLAG_NO_CREATE or PendingIntent.FLAG_UPDATE_CURRENT)
        return pendingIntent != null
    }
}