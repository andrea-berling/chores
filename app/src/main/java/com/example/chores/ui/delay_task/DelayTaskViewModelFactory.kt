package com.example.chores.ui.delay_task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.chores.data.repository.TaskRepository

class DelayTaskViewModelFactory (private val taskRepository: TaskRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DelayTaskViewModel(taskRepository) as T
    }
}