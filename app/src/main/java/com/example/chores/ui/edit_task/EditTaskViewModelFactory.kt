package com.example.chores.ui.edit_task

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.delay_task.DelayTaskViewModel


//interface EditTaskViewModelFactory : ViewModelProvider.Factory

open class EditTaskViewModelFactory (private val taskRepository: TaskRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return EditTaskViewModel(taskRepository) as T
    }
}