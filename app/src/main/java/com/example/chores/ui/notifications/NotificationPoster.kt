package com.example.chores.ui.notifications

import android.app.IntentService
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.Task
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.settings.SettingsValidator
import com.example.chores.ui.show_task.ShowTaskActivity
import com.example.chores.util.Constants
import kotlinx.coroutines.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import kotlin.random.Random

class NotificationPoster : BroadcastReceiver() {

    private fun taskPriorityToNotificationPriority(task: Task): Int {
        val priorityValue = task.priority.ordinal +
                (task.taskClass?.priority?.ordinal ?: throw Exception("Task class can't be null"))
        return when (priorityValue) {
            0 -> NotificationCompat.PRIORITY_MIN
            1 -> NotificationCompat.PRIORITY_LOW
            2,3 -> NotificationCompat.PRIORITY_DEFAULT
            4,5 -> NotificationCompat.PRIORITY_HIGH
            6 -> NotificationCompat.PRIORITY_MAX
            else -> throw Exception("This can't happen")
        }
    }

    private fun postNotification(task: Task) {

        val context = MainApplication.applicationContext()

        val notificationIntent = Intent(context, ShowTaskActivity::class.java).apply {
            putExtra("task",task)
        }
        val notificationId = Random.nextInt()

        val pendingIntent = TaskStackBuilder.create(context).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(notificationIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val markOngoingIntent = Intent(Constants.ACTION_MARK_ONGOING).apply {
            val b = Bundle().apply {
                putParcelable("task",task)
            }
            putExtra("taskBundle",b)
            putExtra("notificationId",notificationId)
            setClass(context,NotificationActionsHandler::class.java)
        }

        val markOngoingPendingIntent = PendingIntent.getBroadcast(context,0,markOngoingIntent,
            PendingIntent.FLAG_UPDATE_CURRENT)

        val markOngoingAction = NotificationCompat.Action.Builder(
            R.drawable.ic_ongoing,context.getString(R.string.mark_ongoing),markOngoingPendingIntent).build()

        val postponeIntent = Intent(Constants.ACTION_DELAY_TASK).apply {
            val b = Bundle().apply {
                putParcelable("task",task)
            }
            putExtra("taskBundle",b)
            putExtra("notificationId",notificationId)
            setClass(context,NotificationActionsHandler::class.java)
        }

        val postponePendingIntent = PendingIntent.getBroadcast(context,0,postponeIntent,
            PendingIntent.FLAG_UPDATE_CURRENT)

        val postponeAction = NotificationCompat.Action.Builder(R.drawable.ic_time,context.getString(R.string.postpone_task),postponePendingIntent).build()

        val largeIcon = BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher)

        val builder = NotificationCompat.Builder(context, Constants.CHORES_NOTIFICATION_CHANNEL)
            .setSmallIcon(R.drawable.ic_chores)
            .setLargeIcon(largeIcon)
            .setContentTitle(task.name)
            .setContentText("The task \"${task.name}\" is due")
            .setPriority(taskPriorityToNotificationPriority(task))
            .setContentIntent(pendingIntent)
            .addAction(markOngoingAction)
            .addAction(postponeAction)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(MainApplication.applicationContext())) {
            notify(notificationId, builder.build())
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.run {
            val task = getBundleExtra("taskBundle")?.getParcelable<Task>("task")
            task?.run {
                postNotification(this)
            } ?: throw java.lang.Exception("Task can't be null")
        }
    }
}

@ExperimentalCoroutinesApi
class NotificationUpdater : IntentService("Notification updater"), CoroutineScope by MainScope(),
    KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val taskRepository : TaskRepository by instance()
    private val notificationCenter : NotificationCenter by instance()
    private val settingsValidator : SettingsValidator by instance()

    @ExperimentalCoroutinesApi
    override fun onDestroy() {
        super.onDestroy()
        cancel()
    }

    override fun onHandleIntent(intent: Intent?) {
        launch {
            val tasks = taskRepository.retrieveFutureTasks(null)
            tasks.forEach{ task ->
                task.taskClass = taskRepository.retrieveClass(task.taskClassId)
                if (settingsValidator.shouldNotify(task))
                {
                    notificationCenter.addNotification(task)
                    Log.d("SETTINGS_DEBUG","for task ${task.id}, gonna add a notification")
                }
                else
                {
                    notificationCenter.removeNotification(task)
                    Log.d("SETTINGS_DEBUG","for task ${task.id}, gonna remove a notification")
                }
            }
        }
    }
}
