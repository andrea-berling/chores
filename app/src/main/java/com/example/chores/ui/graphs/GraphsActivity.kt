package com.example.chores.ui.graphs

import android.content.Intent
import android.os.Bundle
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.example.chores.R
import kotlinx.android.synthetic.main.activity_graphs.*

class GraphsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_graphs)
        setSupportActionBar(findViewById(R.id.toolbar))
        title = getString(R.string.graphs)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        list_view.onItemClickListener = AdapterView.OnItemClickListener { _, _, index, _ ->
            when(GraphType.values()[index]) {
                GraphType.CLASS_PIE_CHART -> {
                   Intent(this,PieChartActivity::class.java).run {
                       startActivity(this)
                   }
                }
                GraphType.DAILY_HISTOGRAM -> {
                    Intent(this,DailyHistogramActivity::class.java).run {
                        startActivity(this)
                    }
                }
                GraphType.WEEKLY_LINE_CHART -> {
                    Intent(this,WeeklyLineChartActivity::class.java).run {
                        startActivity(this)
                    }
                }
            }
        }
    }
}

enum class GraphType {
    CLASS_PIE_CHART, DAILY_HISTOGRAM, WEEKLY_LINE_CHART
}
