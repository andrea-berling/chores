package com.example.chores.ui.notifications

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import com.example.chores.MainApplication
import com.example.chores.data.model.Task

/**
 * NotifcationCenter mock that dispatches the notifications immediately, without scheduling them
 */
class NotificationCenterMock : NotificationCenter {

    private fun buildIntent(task : Task): Intent {
        return Intent(MainApplication.applicationContext(),NotificationPoster::class.java).apply {
            val b = Bundle().apply {
                putParcelable("task", task)
            }
            putExtra("taskBundle", b)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                identifier = task.id
            else
                data = Uri.parse("chores://notifications/${task.id}")
        }
    }

    override fun addNotification(task: Task): Boolean {
        MainApplication.applicationContext().sendBroadcast(buildIntent(task))
        return true
    }

    override fun removeNotification(task: Task): Boolean {
        return true
    }

    override fun updateNotification(task: Task): Boolean {
        return true
    }

    override fun isNotificationSet(task: Task): Boolean {
        return true
    }
}