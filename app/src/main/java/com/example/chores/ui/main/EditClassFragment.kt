package com.example.chores.ui.main

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.TaskClass
import com.example.chores.databinding.CreateClassDialogBinding
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.create_class_dialog.*
import kotlinx.android.synthetic.main.create_class_dialog.view.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class EditClassFragment(private var taskClass: TaskClass?) : DialogFragment(),KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val viewModelFactory: MainActivityViewModelFactory by instance()

    lateinit var binding : CreateClassDialogBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val viewModel = ViewModelProviders.of(this,viewModelFactory)[MainActivityViewModel::class.java]
        binding = DataBindingUtil.inflate(
            activity!!.layoutInflater,R.layout.create_class_dialog,
            null,
            false)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        taskClass?.run {
            viewModel.editTaskClassId = id
            viewModel.editTaskClassName.value = name
            viewModel.editTaskClassPriority.value = priority
        }

        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Set the dialog title
            builder.setTitle(R.string.create_class)
                .setView(binding.root)
                .setPositiveButton(R.string.ok,null)
                .setNegativeButton(R.string.cancel) { _, _ -> dismiss() }

            builder.create().apply {
                /*
                By default the positive button dismisses the dialog
                This way the dialog is dismissed manually
                 */
                setOnShowListener {
                    val positiveButton = getButton(AlertDialog.BUTTON_POSITIVE)
                    positiveButton.setOnClickListener {
                        if(validData(viewModel)) {
                            (activity as EditClassListener).onConfirmClassEdit(
                                // !! shoud be ok, as validData returned true
                                TaskClass(viewModel.editTaskClassId,
                                    viewModel.editTaskClassName.value!!,
                                    viewModel.editTaskClassPriority.value!!
                                ),
                                taskClass == null)
                            dismiss()
                        }
                    }
                }
            }
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun validData(viewModel: MainActivityViewModel): Boolean {

        return if(viewModel.editTaskClassName.value == null || viewModel.editTaskClassName.value == "") {
            binding.root.class_name_layout.error = "Insert a name"
            false
        }
        else
            true
    }
}

interface EditClassListener {

    /**
     * Gets called when a class has been edited, or created, and the confirm button was clicked
     *
     * @param taskClass edited/created class
     * @param newClass flag that indicates whether the class was created (true), or just edited (false)
     */
    fun onConfirmClassEdit(taskClass: TaskClass, newClass: Boolean)
}

