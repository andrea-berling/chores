package com.example.chores.ui.graphs

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.androidplot.xy.*
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.TaskList
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.databinding.ActivityWeeklyLineChartBinding
import com.example.chores.ui.common.DatePickerFragment
import kotlinx.android.synthetic.main.activity_weekly_line_chart.*
import kotlinx.android.synthetic.main.activity_weekly_line_chart.end
import kotlinx.android.synthetic.main.activity_weekly_line_chart.start
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance
import org.kodein.di.android.kodein
import java.lang.Exception
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

class WeeklyLineChartActivity : AppCompatActivity() {

    private lateinit var viewModel: LineChartViewModel
    private lateinit var lineChart: XYPlot

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding : ActivityWeeklyLineChartBinding = DataBindingUtil.setContentView(this,R.layout.activity_weekly_line_chart)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Weekly Line Chart"

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED

        viewModel = ViewModelProviders.of(this)[LineChartViewModel::class.java]

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        lineChart = line_chart

        viewModel.start.value = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
            set(Calendar.MILLISECOND,0)
            add(Calendar.WEEK_OF_YEAR,-4)
        }

        viewModel.end.value = Calendar.getInstance().apply {
            timeInMillis = viewModel.start.value!!.timeInMillis
            add(Calendar.WEEK_OF_YEAR,8)
        }

        center_button.setOnClickListener {
            center()
        }

        addDatePicker(start,viewModel.start)
        addDatePicker(end,viewModel.end)

        lineChart.graph.run {
            getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : NumberFormat() {
                override fun parse(p0: String, p1: ParsePosition): Number? {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun format(
                    p0: Double,
                    p1: StringBuffer,
                    p2: FieldPosition
                ): StringBuffer {
                    val c = Calendar.getInstance().apply {
                        timeInMillis = viewModel.start.value!!.timeInMillis
                    }
                    val buffer = StringBuffer()
                    c.add(Calendar.WEEK_OF_YEAR,p0.toInt())
                    buffer.append(SimpleDateFormat("dd/MM").format(c.timeInMillis))
                    return buffer
                }

                override fun format(
                    p0: Long,
                    p1: StringBuffer,
                    p2: FieldPosition
                ): StringBuffer {
                    return format(p0.toDouble(),p1,p2)
                }
            }
        }

        update_button.setOnClickListener {
            showTasks()
        }

        showTasks()
    }

    private fun showTasks() {
        viewModel.viewModelScope.launch {

            lineChart.clear()

            lateinit var tasksToShow : TaskList
            try {
                tasksToShow = viewModel.retrieveTasks()
            }
            catch (e : Exception) {
                Toast.makeText(this@WeeklyLineChartActivity,e.message, Toast.LENGTH_SHORT).show()
                return@launch
            }

            for(task in tasksToShow)
                Log.d("LINE_DEBUG","task: ${task.name}, status: ${task.status}")

            val seriesValues = viewModel.extractTasksPerWeek(tasksToShow, viewModel.start.value!!, viewModel.end.value!!)

            val series = SimpleXYSeries(
                seriesValues,
                SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,
                "Completed tasks"
            )
            val seriesFormatter = LineAndPointFormatter(
                this@WeeklyLineChartActivity,
                R.xml.line_point_formatter_with_labels
            )

            lineChart.addSeries(series, seriesFormatter)

            PanZoom.attach(lineChart,PanZoom.Pan.HORIZONTAL,PanZoom.Zoom.STRETCH_BOTH)

            center()

            lineChart.redraw()
        }
    }

    private fun addDatePicker(view : View, liveData: MutableLiveData<Calendar>) {
        view.setOnClickListener {
            val dateFragment = DatePickerFragment(liveData,false)
            dateFragment.show(supportFragmentManager, "datePicker")
        }
    }

    private fun center() {

        // The number can't be seen with an upperBound equal to the max. A little offset is needed
        val offset = 5
        lineChart.setDomainBoundaries(-2, 10, BoundaryMode.FIXED)
        lineChart.setRangeBoundaries(0, viewModel.maxValue + offset, BoundaryMode.FIXED)
        lineChart.redraw()
    }
}

class LineChartViewModel : ViewModel(), KodeinAware {

    var maxValue = 0
    override val kodein by kodein(MainApplication.applicationContext())
    private val taskRepository : TaskRepository by instance()

    var start = MutableLiveData<Calendar>()
    var end = MutableLiveData<Calendar>()

    suspend fun retrieveTasksOfRange(start : Calendar, end : Calendar) = taskRepository.retrieveTasksOfRange(start,end)

    fun extractTasksPerWeek(tasks: TaskList, start: Calendar, end: Calendar): List<Int> {
        if (end.timeInMillis >= start.timeInMillis)
        {
            val values = LinkedList<Int>()
            val c1 = Calendar.getInstance().apply {
                timeInMillis = start.timeInMillis
            }
            val c2 = Calendar.getInstance().apply {
                timeInMillis = c1.timeInMillis
            }
            while(c1.timeInMillis < end.timeInMillis) {
                var tmp = 0
                c2.timeInMillis = c1.timeInMillis
                c2.add(Calendar.WEEK_OF_YEAR,1)
                for(task in tasks)
                    if (task.datetime >= c1 && task.datetime < c2)
                        if (task.status == TaskStatus.COMPLETED)
                            tmp++
                if (maxValue < tmp)
                    maxValue = tmp
                values.add(tmp)
                c1.add(Calendar.WEEK_OF_YEAR,1)
            }
            return values
        }
        else
        // TODO just for Debugging
            throw Exception("Invalid date range")
    }

    suspend fun retrieveTasks(): TaskList {
        if (start.value != null && end.value != null)
            if (end.value!! >= start.value!!)
                return retrieveTasksOfRange(start.value!!, end.value!!)
            else
                throw Exception("Invalid date range")
        else
            throw Exception("Invalid date range")
    }
}
