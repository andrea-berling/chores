package com.example.chores.ui.main

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.Menu.NONE
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.chores.R
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskClass
import com.example.chores.data.model.TaskList
import com.example.chores.databinding.ActivityMainBinding
import com.example.chores.ui.common.ConfirmFragment
import com.example.chores.ui.common.ConfirmationListener
import com.example.chores.ui.edit_task.EditTaskActivity
import com.example.chores.ui.graphs.GraphsActivity
import com.example.chores.ui.settings.SettingsActivity
import com.example.chores.ui.show_task.ShowTaskActivity
import com.example.chores.util.Constants
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener,
    ConfirmationListener, EditClassListener, KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory : MainActivityViewModelFactory by instance()

    companion object {
        const val CREATE_TASK_REQUEST = 1
        const val CLASSES_SUBMENU_GROUP = 1
    }

    private lateinit var viewModel : MainActivityViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: TaskAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var canEditDeleteClass = false
    private var classesSubMenuId = -1
    private var futureChoresId = -1
    private var allChoresId = -1
    private var graphsId = -1

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,viewModelFactory)[MainActivityViewModel::class.java]

        val binding : ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            val createTaskIntent = Intent(this, EditTaskActivity::class.java)
            startActivityForResult(createTaskIntent,CREATE_TASK_REQUEST)
        }

        val navView: NavigationView = setupNavigationDrawer(toolbar)

        viewModel.getClassesLiveData().observe(this,
            Observer<List<TaskClass>> { l ->
                if (l != null)
                    populateDrawerWithClasses(l)
                if (viewModel.classIndex >= 0)
                    viewModel.toolBarTitle.value = l[viewModel.classIndex].name
            }
        )

        addNavigationDrawerMenuEntries()

        navView.setNavigationItemSelectedListener(this)

        viewManager = if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            GridLayoutManager(this,2)
        else
            GridLayoutManager(this,3)

        setupRecyclerView()

        viewModel.getTasksLiveData().observe(this, Observer<TaskList> {
            viewAdapter.tasks = it.apply {
                sortWith(Comparator { task1, task2 ->
                    return@Comparator when {
                        task1.datetime > task2.datetime -> 1
                        task1.datetime < task2.datetime -> -1
                        task1.datetime == task2.datetime -> 0
                        else -> 0 // No need for this, but it doesn't compile without it
                    }
                })
            }
            viewAdapter.notifyDataSetChanged()
        })

        showFutureTasks()
    }

    private fun setupRecyclerView() {
        viewAdapter = TaskAdapter(TaskList()).apply {
            setViewHolderOnItemClickListener(object : TaskAdapter.OnItemClickListener {
                override fun onItemClick(v: View, position: Int) {
                    val taskId = this@apply.tasks[position].id
                    viewModel.viewModelScope.launch {
                        val task = withContext(Dispatchers.Default) {
                            viewModel.retrieveTaskWithEverything(taskId)
                        }
                        Intent(this@MainActivity, ShowTaskActivity::class.java).run {
                            putExtra("task", task)
                            startActivity(this)
                        }
                    }
                }
            })
        }

        recyclerView = my_recycler_view.apply {
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter
        }
    }

    private fun setupNavigationDrawer(toolbar: Toolbar): NavigationView {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        return navView
    }

    private fun addNavigationDrawerMenuEntries() {
        futureChoresId = View.generateViewId()
        allChoresId = View.generateViewId()
        graphsId = View.generateViewId()
        classesSubMenuId = View.generateViewId()
        nav_view.menu.add(NONE, futureChoresId, NONE, "Future Chores")
        nav_view.menu.add(NONE, allChoresId, NONE, "All Chores")
        nav_view.menu.addSubMenu(NONE, classesSubMenuId, NONE, R.string.classes)
        nav_view.menu.addSubMenu("Stats").add(NONE, graphsId, NONE, R.string.graphs)
    }

    override fun onResume() {
        super.onResume()
        viewModel.updateTasks()
    }

    private fun populateDrawerWithClasses(taskClassList: List<TaskClass>) {
        val menu = nav_view.menu
        // submenu should never be null, as classesSubMenu is initialized in addNavigationDrawerMenuEntries
        menu.findItem(classesSubMenuId).subMenu.run {
            clear()
            for ((i,taskClass) in taskClassList.withIndex())
                add(CLASSES_SUBMENU_GROUP,i,i,taskClass.name)

            nav_view.invalidate()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                CREATE_TASK_REQUEST -> {
                    if (data != null && data.extras != null)
                    {
                        val task = data.extras!!.getParcelable<Task>("result")
                        task?.run {
                            viewModel.createTask(task)
                        }
                    }
                }
            }

        }
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.action_delete_class)?.isEnabled = canEditDeleteClass
        menu?.findItem(R.id.action_edit_class)?.isEnabled = canEditDeleteClass
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> {
                startActivity(Intent(this,SettingsActivity::class.java))
                true
            }
            R.id.action_add_class -> {
                EditClassFragment(null).run {
                    this.show(this@MainActivity.supportFragmentManager,"Create class")
                }
                return true
            }
            R.id.action_delete_class -> {
                ConfirmFragment(
                    R.string.confirm_class_removal_title,
                    R.string.confirm_class_removal_message
                ).run {
                    show(this@MainActivity.supportFragmentManager,"Delete class")
                }
                return true
            }
            R.id.action_edit_class -> {
                // !! should be ok, we should have classes if this button is enabled and clicked
                EditClassFragment(viewModel.classes.value!![viewModel.classIndex]).run {
                    this.show(this@MainActivity.supportFragmentManager,"Create class")
                }
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when {
            item.groupId == CLASSES_SUBMENU_GROUP -> {
                viewModel.getClassWithIndexTasksLiveData(item.itemId)
                viewModel.toolBarTitle.value = viewModel.classes.value!![item.itemId].name
                canEditDeleteClass = viewModel.classes.value!![item.itemId].id != Constants.DEFAULT_CLASS_ID
                invalidateOptionsMenu()
            }
            item.itemId == allChoresId -> showAllTasks()
            item.itemId == graphsId -> Intent(this,GraphsActivity::class.java).run {
                startActivity(this)
            }
            item.itemId == futureChoresId -> showFutureTasks()
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showFutureTasks() {
        viewModel.getFutureTasksLiveData()
        viewModel.toolBarTitle.value = "Future Chores"
        canEditDeleteClass = false
        invalidateOptionsMenu()
    }

    private fun showAllTasks() {
        viewModel.getAllTasksLiveData()
        viewModel.toolBarTitle.value = "Chores"
        canEditDeleteClass = false
        invalidateOptionsMenu()
    }

    override fun onConfirmation(confirmation: Boolean) {
        if (confirmation) {
            viewModel.viewModelScope.launch {
                viewModel.deleteCurrentClass()
                showFutureTasks()
            }
        }
    }

    override fun onConfirmClassEdit(taskClass: TaskClass, newClass: Boolean) {
        viewModel.addClass(taskClass,newClass)
    }
}
