package com.example.chores.ui.edit_task

import androidx.lifecycle.*
import com.example.chores.data.model.*
import com.example.chores.data.repository.FreshId
import com.example.chores.data.repository.TaskRepository
import java.util.*
import kotlin.collections.ArrayList

class EditTaskViewModel(private val taskRepository: TaskRepository): ViewModel() {

    var id = ""
    var name = MutableLiveData<String>()
    var description = MutableLiveData<String>()
    var classes = ArrayList<TaskClass>()
    var classIndex = MutableLiveData<Int>()
    var priority = MutableLiveData<TaskPriority>()
    var date = MutableLiveData<Calendar>()
    var notificationTime = MutableLiveData<Int>()
    var notificationUnit = MutableLiveData<NotificationUnit>()
    var afterTasks = ArrayList<Task>()
    var selectedAfterTasks = ArrayList<Int>()
    var status = TaskStatus.PENDING

    suspend fun retrieveClasses(classIdToSet : String) : List<TaskClass> {
        classes = ArrayList(taskRepository.retrieveClasses())
        for ((i, _class) in classes.withIndex()) {
            if (classIdToSet.isNotEmpty() && classIdToSet == _class.id) {
                classIndex.value = i
                break
            }
        }
        return classes
    }

    suspend fun retrieveFutureTasks(tasksToSelect: TaskList?) {
        val futureTasks = taskRepository.retrieveFutureTasks(if (id.isNotEmpty()) id else null)
        futureTasks.forEach{ task ->
            if (id.isEmpty() || task.id != id)
                afterTasks.add(task)
        }
        tasksToSelect?.forEach {
            var idx = indexOfWithId(it.id,afterTasks)
            /* Tasks that have been added as a dependency may be in the past at the moment the
            modification takes place. In this case, we re-insert them, so that the user can remove
            them from the dependencies
             */
            if (idx < 0) {
                idx = afterTasks.size
                afterTasks.add(it)
            }

            selectedAfterTasks.add(idx)
        }
    }

    private fun indexOfWithId(taskId: String, afterTasks: java.util.ArrayList<Task>): Int {
        for ((i,task) in afterTasks.withIndex())
            if(task.id == taskId)
                return i
        return -1
    }

    suspend fun createTask(): Task {
        val name = name.value!!
        val description = description.value ?: ""
        val priority = priority.value!!
        /* A default class is always defined, classes array can't be null */
        val taskClass = classes[classIndex.value!!]
        val calendar = date.value!!
        val notificationTime =  notificationTime.value!!
        val notificationUnit = notificationUnit.value!!
        val id = if (id.isNotEmpty()) id else taskRepository.getFreshId(FreshId.TASK)

        val task = Task(
                id,
                name,
                description,
                priority,
                taskClass.id,
                calendar,
                notificationTime,
                notificationUnit,
                status
        )
        task.taskClass = taskClass
        val tmpAfterTasks = TaskList()
        for ((index,element ) in afterTasks.withIndex())
            if (selectedAfterTasks.contains(index))
                tmpAfterTasks.add(element)
        task.afterTasks = if (tmpAfterTasks.isNotEmpty()) tmpAfterTasks else null

        return task
    }

    fun setTask(task: Task) {
        id = task.id
        name.value = task.name
        description.value = task.description
        priority.value = task.priority
        date.value = task.datetime
        notificationTime.value = task.notificationTime
        notificationUnit.value = task.notificationUnit
        status = task.status
    }

}
