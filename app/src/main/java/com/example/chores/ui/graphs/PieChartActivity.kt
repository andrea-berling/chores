package com.example.chores.ui.graphs

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.androidplot.pie.PieChart
import com.androidplot.pie.PieRenderer
import com.androidplot.pie.Segment
import com.androidplot.pie.SegmentFormatter
import com.androidplot.util.PixelUtils
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.TaskClass
import com.example.chores.data.repository.TaskRepository
import kotlinx.android.synthetic.main.activity_pie_chart.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class PieChartActivity : AppCompatActivity() {

    private lateinit var viewModel: PieChartViewModel
    private lateinit var pieChart: PieChart

    @ExperimentalUnsignedTypes
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pie_chart)
        viewModel = ViewModelProviders.of(this)[PieChartViewModel::class.java]
        title = "Classes Pie Chart"

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        pieChart = pie_chart

        viewModel.viewModelScope.launch {
            val classes = viewModel.retrieveClasses()
            val n = classes.size
            if (n > 0) {
                val segmentList = LinkedList<Pair<Segment,SegmentFormatter>>()
                val step : UInt = if (n > 1) {PieChartViewModel.ALL_COLORS / (n - 1).toUInt()} else 0U
                for((i,taskClass) in classes.withIndex()) {
                    val numOfTasks = viewModel.getTaskNumberOfClass(taskClass.id)
                    val segment = Segment(taskClass.name,numOfTasks)
                    val segmentFormatter =
                        if (i < n) SegmentFormatter(PieChartViewModel.OPAQUE or (i * step.toInt()))
                        else SegmentFormatter(PieChartViewModel.LAST)
                    segmentFormatter.labelPaint.textSize = PixelUtils.dpToPix(16f)
                    //segmentFormatter.labelPaint.color = Color.GREEN
                    segmentFormatter.labelPaint.setShadowLayer(5f, 0f, 0f, Color.BLACK)
                    segmentList.add(Pair(segment,segmentFormatter))
                }


                segmentList.forEach{
                    pieChart.addSegment(it.first,it.second)
                }

                pieChart.getRenderer(PieRenderer::class.java)?.run {
                    setDonutSize(0F,PieRenderer.DonutMode.PERCENT)
                }

                pieChart.redraw()
            }
        }
    }
}



class PieChartViewModel : ViewModel(),KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val taskRepository : TaskRepository by instance()

    companion object {
        const val OPAQUE = Color.BLACK
        const val LAST = 0xffffffff.toInt()
        @ExperimentalUnsignedTypes
        val ALL_COLORS = 0x00ffffff.toUInt()
    }

    suspend fun retrieveClasses() : List<TaskClass> = taskRepository.retrieveClasses()
    suspend fun getTaskNumberOfClass(id: String): Int = taskRepository.retrieveTaskNumberOfClass(id)
}
