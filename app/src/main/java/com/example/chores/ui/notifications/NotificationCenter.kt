package com.example.chores.ui.notifications

import android.app.*
import android.app.AlarmManager.RTC_WAKEUP
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.Task
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.settings.SettingsValidator
import com.example.chores.ui.show_task.ShowTaskActivity
import com.example.chores.util.Constants.ACTION_DELAY_TASK
import com.example.chores.util.Constants.ACTION_MARK_ONGOING
import com.example.chores.util.Constants.CHORES_NOTIFICATION_CHANNEL
import kotlinx.coroutines.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random

interface NotificationCenter {

    /**
     * Schedules a notification for the given task, in accordance with the user defined settings
     *
     * @param task Task for which the notification will be scheduled
     * @return true if the notification was added; false otherwise
     */
    fun addNotification(task : Task) : Boolean

    /**
     * Removes a scheduled notification for the given task, if registered
     *
     * @param task Task for which the notification will be removed
     * @return true if the notification was removed; false otherwise
     */
    fun removeNotification(task : Task) : Boolean

    /**
     * Updates a scheduled notification for the given task, if registered, in accordance to the
     * user defined settings
     *
     * @param task Task for which the notification will be updated
     * @return true if the notification was updated; false otherwise
     */
    fun updateNotification(task : Task) : Boolean

    /**
     * Returns whether or not a notification has been scheduled for the given task
     *
     * @param task Task fow which the notification will be checked
     * @return true if the notification for the task was scheduled
     */
    fun isNotificationSet(task : Task) : Boolean
}
