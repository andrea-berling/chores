package com.example.chores.ui.settings

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import androidx.preference.MultiSelectListPreference
import androidx.preference.PreferenceFragmentCompat
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.TaskClass
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.notifications.NotificationUpdater
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class SettingsActivity : AppCompatActivity() {

    lateinit var viewModel : SettingsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        setSupportActionBar(findViewById(R.id.toolbar))

        viewModel = ViewModelProviders.of(this)[SettingsViewModel::class.java]

        val fragment = SettingsFragment()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, fragment)
            .commit()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.viewModelScope.launch {
            val classes = viewModel.retrieveClasses()
            val classesPreference = fragment.findPreference<MultiSelectListPreference>("notification_class_filter")
            classesPreference?.run {
                entries = classes.map { taskClass -> taskClass.name }.toTypedArray()
                entryValues = classes.map { taskClass -> taskClass.id }.toTypedArray()
            }
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }

    @ExperimentalCoroutinesApi
    override fun onDestroy() {
        super.onDestroy()
        // Updating the notifications for the tasks
        if (isFinishing){
            startService(Intent(MainApplication.applicationContext(),NotificationUpdater::class.java))
        }
    }
}

class SettingsViewModel: ViewModel(), KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())

    private val taskRepository : TaskRepository by instance()

    suspend fun retrieveClasses() : List<TaskClass> = taskRepository.retrieveClasses()
}