package com.example.chores.ui.graphs

import android.content.pm.ActivityInfo
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.androidplot.util.PixelUtils
import com.androidplot.xy.*
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.TaskList
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.databinding.ActivityDailyHistogramBinding
import com.example.chores.ui.common.DatePickerFragment
import kotlinx.android.synthetic.main.activity_daily_histogram.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.lang.Exception
import java.text.FieldPosition
import java.text.NumberFormat
import java.text.ParsePosition
import java.text.SimpleDateFormat
import java.util.*

class DailyHistogramActivity : AppCompatActivity() {

    private lateinit var barChart : XYPlot
    private lateinit var viewModel: DailyHistogramViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityDailyHistogramBinding = DataBindingUtil.setContentView(this, R.layout.activity_daily_histogram)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[DailyHistogramViewModel::class.java]

        binding.viewModel = viewModel

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Daily Histogram"
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LOCKED

        barChart = bar_chart

        center_button.setOnClickListener {
            center()
        }

        addDatePicker(start,viewModel.start)
        addDatePicker(end,viewModel.end)

        viewModel.start.value = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
            set(Calendar.MILLISECOND,0)
        }

        viewModel.end.value = Calendar.getInstance().apply {
            timeInMillis = viewModel.start.value!!.timeInMillis
            add(Calendar.DAY_OF_YEAR,7)
        }

        update_button.setOnClickListener {
            showTasks()
        }

        showTasks()
    }

    private fun showTasks() {
        viewModel.viewModelScope.launch {

            barChart.clear()
            lateinit var tasksToShow : TaskList
            try {
                tasksToShow = viewModel.retrieveTasks()
            }
            catch (e : Exception) {
                Toast.makeText(this@DailyHistogramActivity,e.message,Toast.LENGTH_SHORT).show()
                return@launch
            }

            val seriesValues = viewModel.extractTasksPerDay(tasksToShow,
                viewModel.start.value!!,
                viewModel.end.value!!)

            val pendingSeries = SimpleXYSeries(seriesValues.map { it.first },SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,"Pending")
            val ongoingSeries = SimpleXYSeries(seriesValues.map { it.second },SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,"Ongoing")
            val completedSeries = SimpleXYSeries(seriesValues.map { it.third },SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,"Completed")

            val pendingFormatter = BarFormatter(Color.RED,Color.BLACK)
            val ongoingFormatter = BarFormatter(Color.YELLOW,Color.BLACK)
            val completedFormatter = BarFormatter(Color.GREEN,Color.BLACK)

            barChart.addSeries(pendingSeries,pendingFormatter)
            barChart.addSeries(ongoingSeries,ongoingFormatter)
            barChart.addSeries(completedSeries,completedFormatter)

            PanZoom.attach(barChart, PanZoom.Pan.HORIZONTAL, PanZoom.Zoom.NONE, PanZoom.ZoomLimit.MIN_TICKS)

            barChart.getRenderer(BarRenderer::class.java).run {
                barOrientation = BarRenderer.BarOrientation.SIDE_BY_SIDE
                setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_WIDTH, PixelUtils.dpToPix(20f))
            }

            barChart.graph.run {
                getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).format = object : NumberFormat() {
                    override fun parse(p0: String, p1: ParsePosition): Number? {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun format(
                        p0: Double,
                        p1: StringBuffer,
                        p2: FieldPosition
                    ): StringBuffer {
                        val c = Calendar.getInstance().apply {
                            timeInMillis = viewModel.start.value!!.timeInMillis
                        }
                        val buffer = StringBuffer()
                        c.add(Calendar.DAY_OF_YEAR,p0.toInt())
                        buffer.append(SimpleDateFormat("dd/MM").format(c.timeInMillis))
                        return buffer
                    }

                    override fun format(
                        p0: Long,
                        p1: StringBuffer,
                        p2: FieldPosition
                    ): StringBuffer {
                        return format(p0.toDouble(),p1,p2)
                    }
                }
            }

            barChart.setDomainStep(StepMode.INCREMENT_BY_VAL,1.0)
            barChart.setRangeStep(StepMode.INCREMENT_BY_VAL,3.0)

            center()
        }

    }

    private fun center() {

        barChart.setDomainBoundaries(-2, 10, BoundaryMode.FIXED)
        barChart.setRangeBoundaries(0, viewModel.maxValue, BoundaryMode.FIXED)
        barChart.redraw()
    }

    private fun addDatePicker(view : View, liveData: MutableLiveData<Calendar>) {
        view.setOnClickListener {
            val dateFragment = DatePickerFragment(liveData,false)
            dateFragment.show(supportFragmentManager, "datePicker")
        }
    }
}

class DailyHistogramViewModel : ViewModel(), KodeinAware {

    var maxValue = 0
    override val kodein by kodein(MainApplication.applicationContext())

    private val taskRepository : TaskRepository by instance()

    var start = MutableLiveData<Calendar>()
    var end = MutableLiveData<Calendar>()

    private suspend fun retrieveTasksOfRange(start : Calendar, end : Calendar ) = taskRepository.retrieveTasksOfRange(start,end)

    /**
     * Extracts the tasks per day, as an array of triple "# pending, # ongoing, #completed"
     */
    fun extractTasksPerDay(tasks: TaskList, start: Calendar, end: Calendar): List<Triple<Int,Int,Int>> {
        if (end.timeInMillis >= start.timeInMillis)
        {
            val values = LinkedList<Triple<Int,Int,Int>>()
            val c1 = Calendar.getInstance().apply {
                timeInMillis = start.timeInMillis
            }
            val c2 = Calendar.getInstance().apply {
                timeInMillis = c1.timeInMillis
            }
            while(c1.timeInMillis < end.timeInMillis) {
                var first = 0
                var second = 0
                var third = 0
                c2.timeInMillis = c1.timeInMillis
                c2.add(Calendar.DAY_OF_YEAR,1)
                for(task in tasks)
                    if (task.datetime >= c1 && task.datetime < c2)
                        when (task.status)
                        {
                            TaskStatus.PENDING -> first++
                            TaskStatus.ONGOING -> second++
                            TaskStatus.COMPLETED -> third++
                        }

                values.add(Triple(first,second, third))
                if (maxValue < first)
                    maxValue = first
                if (maxValue < second)
                    maxValue = second
                if (maxValue < third)
                    maxValue = third
                c1.add(Calendar.DAY_OF_YEAR,1)
            }
            return values
        }
        else
            // TODO just for Debugging
            throw Exception("Invalid date range")
    }

    suspend fun retrieveTasks(): TaskList {
        if (start.value != null && end.value != null)
            if (end.value!! >= start.value!!)
                return retrieveTasksOfRange(start.value!!, end.value!!)
            else
                throw Exception("Invalid date range")
        else
            throw Exception("Invalid date range")
    }
}
