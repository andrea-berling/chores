package com.example.chores.ui.settings

import android.util.Log
import androidx.preference.PreferenceManager
import com.example.chores.MainApplication
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.model.TaskStatus

class SettingsValidatorImpl : SettingsValidator {

    override fun shouldNotify(task : Task) : Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(MainApplication.applicationContext())
        if(preferences.getBoolean("notification_priority",false))
        {
            val allowedPriorityStrings = preferences.getStringSet("notification_priority_filter", HashSet<String>())!!
            val allowedPriorities = HashSet<TaskPriority>()
            allowedPriorityStrings.forEach{
                allowedPriorities.add(TaskPriority.valueOf(it))
            }
            if (!allowedPriorities.contains(task.priority))
                return false
        }
        if(preferences.getBoolean("notification_class_priority",false))
        {
            val allowedPriorityStrings = preferences.getStringSet("notification_class_priority_filter", HashSet<String>())!!
            val allowedPriorities = HashSet<TaskPriority>()
            allowedPriorityStrings.forEach{
                allowedPriorities.add(TaskPriority.valueOf(it))
            }
            Log.d("CLASS_PRIO_FILTER","task: $task")
            if (!allowedPriorities.contains(task.taskClass!!.priority))
                return false
        }
        if(preferences.getBoolean("notification_class",false))
        {
            val allowedClasses = preferences.getStringSet("notification_class_filter", HashSet<String>())!!
            if (!allowedClasses.contains(task.taskClassId))
                return false
        }

        return true
    }

    override fun shouldShow(task : Task) : Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(MainApplication.applicationContext())

        if(preferences.getBoolean("todo",false) && task.status == TaskStatus.COMPLETED)
            return false

        if(preferences.getBoolean("filter_priority",false))
        {
            val allowedPriorityStrings = preferences.getStringSet("priority_filter", HashSet<String>())!!
            val allowedPriorities = HashSet<TaskPriority>()
            allowedPriorityStrings.forEach{
                allowedPriorities.add(TaskPriority.valueOf(it))
            }
            if (!allowedPriorities.contains(task.priority))
                return false
        }

        return true
    }
}