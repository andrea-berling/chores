package com.example.chores.ui.edit_task

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.example.chores.R

class AfterTasksPickerFragment : DialogFragment() {

    lateinit var model: EditTaskViewModel

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        model = activity?.run {
            ViewModelProviders.of(this)[EditTaskViewModel::class.java]
        } ?: throw Exception("Invalid Activity")
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            // Set the dialog title
            builder.setTitle(R.string.pick_after_tasks)
                // Specify the list array, the items to be selected by default (null for none),
                // and the listener through which to receive callbacks when items are selected
                .setMultiChoiceItems(model.afterTasks.map{ task -> task.name }.toTypedArray(), fromIndicesToBooleans(model.selectedAfterTasks,model.afterTasks.size)
                ) { _, which, isChecked ->
                    if (isChecked) {
                        // If the user checked the item, add it to the selected items
                        if (!model.selectedAfterTasks.contains(which))
                            model.selectedAfterTasks.add(which)
                        Log.d("DEBUG_AFTERTASKS","Selected tasks: ${model.selectedAfterTasks}")
                    } else if (model.selectedAfterTasks.contains(which)) {
                        // Else, if the item is already in the array, remove it
                        Log.d("DEBUG_AFTERTASKS","Selected tasks: ${model.selectedAfterTasks}")
                        model.selectedAfterTasks.remove(Integer.valueOf(which))
                    }
                }
                // Set the action buttons
                .setPositiveButton(R.string.close
                ) { _, _ ->
                    // User clicked OK, so save the selectedItems results somewhere
                    // or return them to the component that opened the dialog
                    dismiss()
                }

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    private fun fromIndicesToBooleans(selected_after_tasks: ArrayList<Int>, size: Int): BooleanArray? {
        return if (selected_after_tasks.isNotEmpty()) {
            //Log.d("AFTER_TASK",selectedAfterTasks.toString())
            val array: BooleanArray? = BooleanArray(size = size)
            for (i in array!!.indices)
                array[i] = false
            for (i in selected_after_tasks)
                array[i] = true
            array
        } else
            null
    }
}