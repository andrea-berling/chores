package com.example.chores.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chores.R
import com.example.chores.data.model.TaskList
import kotlinx.android.synthetic.main.task_card_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class TaskAdapter(var tasks: TaskList) : RecyclerView.Adapter<TaskAdapter.ViewHolder>(){
    private lateinit var listener: OnItemClickListener

    interface OnItemClickListener {
        fun onItemClick(v: View, position: Int)
    }

    class ViewHolder(itemView: View, private var listener: OnItemClickListener) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION)
                listener.onItemClick(itemView, position)
        }

        var title : TextView = itemView.findViewById(R.id.title)
        var description: TextView = itemView.findViewById(R.id.description)
        var date: TextView = itemView.date_text
        var time: TextView = itemView.time_text

        init {
            itemView.setOnClickListener(this)
        }

        fun setOnItemClickListener(l: OnItemClickListener) {
            listener = l
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.task_card_layout,parent,false)
        val viewHolder = ViewHolder(view, listener)
        view.setOnClickListener(viewHolder)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        holder.title.text = task.name
        holder.description.text = task.description
        holder.date.text = SimpleDateFormat("dd/MM/yyyy", Locale.ITALY).format(task.datetime.timeInMillis)
        holder.time.text = SimpleDateFormat("HH:mm",Locale.ITALY).format(task.datetime.timeInMillis)
    }

    fun setViewHolderOnItemClickListener(l: OnItemClickListener) {
        listener = l
    }
}