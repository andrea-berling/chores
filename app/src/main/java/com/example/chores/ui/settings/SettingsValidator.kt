package com.example.chores.ui.settings

import com.example.chores.data.model.Task

interface SettingsValidator {

    /**
     * Returns whether or not the given task should be notified
     *
     * @param task task to verify
     * @return true if the task should be notified; false otherwise
     */
    fun shouldNotify(task : Task) : Boolean

    /**
     * Returns whether or not the given task should be shown
     *
     * @param task task to verify
     * @return true if the task should be shown; false otherwise
     */
    fun shouldShow(task : Task) : Boolean
}