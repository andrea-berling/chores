package com.example.chores.ui.common

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.MutableLiveData
import java.util.*

class DatePickerFragment(private val liveData: MutableLiveData<Calendar>, private val onlyFutureDates : Boolean) : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val c = if (liveData.value == null)
            Calendar.getInstance()
        else
            liveData.value!!

        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Create a new instance of DatePickerDialog and return it
        return DatePickerDialog(activity!!, this, year, month, day).apply {
            if (onlyFutureDates)
                datePicker.minDate = Calendar.getInstance().timeInMillis
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val c = if (liveData.value == null)
            Calendar.getInstance()
        else
            liveData.value!!
        c.set(year,month,dayOfMonth)
        liveData.value = c
    }
}
