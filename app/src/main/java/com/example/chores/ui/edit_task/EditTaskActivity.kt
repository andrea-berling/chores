package com.example.chores.ui.edit_task

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import com.example.chores.R
import com.example.chores.data.model.NotificationUnit
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskList
import com.example.chores.databinding.ActivityEditTaskBinding
import com.example.chores.ui.common.DatePickerFragment
import com.example.chores.ui.common.TimePickerFragment
import kotlinx.android.synthetic.main.activity_edit_task.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.Calendar

class EditTaskActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory: EditTaskViewModelFactory by instance()

    private lateinit var viewModel: EditTaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this,viewModelFactory)[EditTaskViewModel::class.java]
        val binding: ActivityEditTaskBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_task)

        setSupportActionBar(findViewById(R.id.toolbar))

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        //viewModel.date.value = Calendar.getInstance()
        viewModel.notificationUnit.value = NotificationUnit.MINUTES
        viewModel.notificationTime.value = 0

        /* Opening the pickers for time and data */
        addDatePicker()

        addTimePicker()

        var classId = ""
        var afterTasksToSelect : TaskList? = null

        val t = intent.getParcelableExtra<Task?>("task")
        if (t == null) /* New task */
        {
            this.setTitle(R.string.create_task)
        }
        else
        {
            this.setTitle(R.string.edit_task)
            viewModel.setTask(t)
            /* This sets the class spinner */
            classId = t.taskClassId
            /* This sets the selected after tasks */
            afterTasksToSelect = t.afterTasks
        }

        /* Reacting to changes in the classes list of the ViewModel */
        bindClasses(classId)

        /* FIXME Right now the registration of the callback for the click event of the after task button is delayed after the retrieval
        of tasks from the database. There is a period of time in which the button is unresponsive. Gotta find a better solution

        There doesn't seem to be a better solution. Fuck Android
         */
        bindAfterTasks(afterTasksToSelect)

        setUpErrorClearing()

        cancel_button.setOnClickListener { finish() }

        addConfirmButtonListener()
    }

    /**
     * Clears the errors displayed in the UI when invalid data was supplied and the user supplies new
     * data
     */
    private fun setUpErrorClearing() {
        viewModel.date.observe(this, Observer {
            it?.run {
                if(pick_date_layout.error != null)
                {
                    pick_date_layout.error = null
                    pick_date_layout.isErrorEnabled = false
                }
                if(pick_time_layout.error != null) {
                    pick_time_layout.error = null
                    pick_time_layout.isErrorEnabled = false
                }
            }
        })
        viewModel.name.observe(this, Observer {
            it?.run {
                if(task_name_layout.error != null) {
                    task_name_layout.error = null
                    task_name_layout.isErrorEnabled = false
                }
            }
        })
        viewModel.notificationTime.observe(this, Observer {
            it?.run {
                if(notification_time_layout.error != null)
                {
                    notification_time_layout.error = null
                    notification_time_layout.isErrorEnabled = false
                }
            }
        })
    }

    private fun addConfirmButtonListener() {
        confirm_button.setOnClickListener {
            if (dataIsValid(viewModel)) {
                viewModel.viewModelScope.launch {
                    val task = viewModel.createTask()
                    Intent().run {
                        setResult(Activity.RESULT_OK,this)
                        putExtra("result",task)
                    }
                    finish()
                }
            }
        }
    }

    private fun dataIsValid(editTaskModel: EditTaskViewModel): Boolean {
        var valid = true
        editTaskModel.name.value?.run {
            if(isEmpty()){
                task_name_layout.error = "Insert a name"
                valid = false
            }

            if(length > 20){
                task_name_layout.error = "Task name is too long"
                valid = false
            }
        } ?: run { task_name_layout.error = "Insert a name"; valid = false }

        val notificationUnit = editTaskModel.notificationUnit.value!!

        editTaskModel.notificationTime.value?.run {
            if ((notificationUnit == NotificationUnit.MINUTES && (this < 0 || this > 59))
                || (notificationUnit == NotificationUnit.HOURS && (this < 0 || this > 24)))
            {
                notification_time_layout.error = "Invalid value for notification time"
                valid = false
            }
        } ?: run {editTaskModel.notificationTime.value = 0}

        editTaskModel.date.value?.run {
            if (!isSet(Calendar.YEAR) || !isSet(Calendar.MONTH) || !isSet(Calendar.DAY_OF_MONTH))
            {
                pick_date_layout.error = "Pick a date"
                valid = false
            }
            if (!isSet(Calendar.HOUR) || !isSet(Calendar.MINUTE))
            {
                pick_time_layout.error = "Pick a time"
                valid = false
            }
            if (timeInMillis < Calendar.getInstance().timeInMillis)
            {
                pick_time_layout.error = "Due time is in the past"
                valid = false
            }
        } ?: run {
            pick_date_layout.error = "Pick a date"
            pick_time_layout.error = "Pick a time"
            valid = false
        }

        return valid
    }

    private fun bindClasses(classId : String) {
        viewModel.viewModelScope.launch {
            val classes = viewModel.retrieveClasses(classId)
            ArrayAdapter(
                this@EditTaskActivity,
                android.R.layout.simple_spinner_item,
                classes.map { _class -> _class.name }
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                class_spinner.adapter = adapter
            }
        }
    }

    private fun bindAfterTasks(tasksToSelect: TaskList?) {
        after_button.setOnClickListener{
            Toast.makeText(this,"Still loading future tasks. Try again later",Toast.LENGTH_SHORT).show()
        }
        viewModel.viewModelScope.launch {
            viewModel.retrieveFutureTasks(tasksToSelect)
            withContext(Dispatchers.Main){
                after_button.setOnClickListener {
                    val fragment = AfterTasksPickerFragment()
                    fragment.show(supportFragmentManager, "After tasks picker")
                }
            }
        }
    }

    private fun addDatePicker() {
        pick_date.setOnClickListener {
            val dateFragment = DatePickerFragment(viewModel.date,true)
            dateFragment.show(supportFragmentManager, "datePicker")
        }
    }

    private fun addTimePicker() {
        pick_time.setOnClickListener {
            val timeFragment = TimePickerFragment(viewModel.date)
            timeFragment.show(supportFragmentManager, "timePicker")
        }
    }

}
