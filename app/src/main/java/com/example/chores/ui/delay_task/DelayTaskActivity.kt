package com.example.chores.ui.delay_task

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.example.chores.R
import com.example.chores.data.model.Task
import com.example.chores.databinding.ActivityDelayTaskBinding
import com.example.chores.ui.common.DatePickerFragment
import com.example.chores.ui.common.TimePickerFragment
import kotlinx.android.synthetic.main.activity_delay_task.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DelayTaskActivity : AppCompatActivity(),KodeinAware {

    override val kodein by kodein()
    private val viewModelFactory : DelayTaskViewModelFactory by instance()


    private lateinit var viewModel: DelayTaskViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.title = getString(R.string.postpone_task)

        viewModel = ViewModelProviders.of(this,viewModelFactory).get(DelayTaskViewModel::class.java)
        val binding: ActivityDelayTaskBinding = DataBindingUtil.setContentView(this, R.layout.activity_delay_task)

        setSupportActionBar(findViewById(R.id.toolbar))

        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        viewModel.task = intent.extras?.getParcelable("task") ?: throw Exception("Task can't be null")
        viewModel.date.value = viewModel.task.datetime
        viewModel.notificationTime.value = viewModel.task.notificationTime
        viewModel.notificationUnit.value = viewModel.task.notificationUnit

        addDatePicker()
        addTimePicker()


        set_time_button.setOnClickListener {
            if (viewModel.date.value != null)
            {
                viewModel.updateTaskTime()
                finish()
            }
            else
            {
                pick_date.error = "Pick a date"
                pick_date.error = "Pick a time"
            }
        }
    }

    private fun addDatePicker() {
        pick_date.setOnClickListener {
            pick_date_layout.error = null
            pick_date_layout.isErrorEnabled = false
            val dateFragment = DatePickerFragment(viewModel.date,true)
            dateFragment.show(supportFragmentManager, "datePicker")
        }
    }

    private fun addTimePicker() {
        pick_time.setOnClickListener {
            pick_time_layout.error = null
            pick_time_layout.isErrorEnabled = false
            val timeFragment = TimePickerFragment(viewModel.date)
            timeFragment.show(supportFragmentManager, "timePicker")
        }
    }
}
