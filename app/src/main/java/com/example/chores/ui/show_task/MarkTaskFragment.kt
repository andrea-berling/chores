package com.example.chores.ui.show_task

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.example.chores.R
import com.example.chores.data.model.TaskStatus

class MarkTaskFragment : DialogFragment() {

    interface SetStatusListener {
        fun onStatusSet(taskStatus: TaskStatus)
    }

    lateinit var listener : SetStatusListener
    private var choice : Int = 0

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val viewModel = activity?.run {
            ViewModelProviders.of(this)[ShowTaskViewModel::class.java]} ?: throw Exception("Activity doesn't have a ViewModel")
        choice = viewModel.status.value!!.ordinal

        return activity?.let{
           return AlertDialog.Builder(it).setTitle(getString(R.string.pick_priority))
               .setSingleChoiceItems(R.array.statuses,viewModel.status.value!!.ordinal) { _, which -> choice = which }
               .setPositiveButton(R.string.ok) { _, _ -> listener.onStatusSet(TaskStatus.values()[choice]) }
               .setNegativeButton(R.string.cancel,null)
               .create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = context as SetStatusListener
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            throw ClassCastException((context.toString() +
                    " must implement SetStatusListener"))
        }

    }
}