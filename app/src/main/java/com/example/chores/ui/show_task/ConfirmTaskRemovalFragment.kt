package com.example.chores.ui.show_task

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.chores.ui.common.ConfirmationListener
import com.example.chores.R
import java.lang.Exception

class ConfirmTaskRemovalFragment : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            // Use the Builder class for convenient dialog construction
            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.confirm_removal_title)
                .setMessage(R.string.confirm_removal_message)
                .setPositiveButton(R.string.confirm
                ) { _, _ ->
                    try {
                        (activity as ConfirmationListener).onConfirmation(true)
                    }
                    catch (ex: Exception)
                    {
                        throw Exception("Activity must Implement ConfirmationListener interface")
                    }
                }
                .setNegativeButton(R.string.cancel
                ) { _, _ ->
                    try {
                        (activity as ConfirmationListener).onConfirmation(false)
                    }
                    catch (ex: Exception)
                    {
                        throw Exception("Activity must Implement ConfirmationListener interface")
                    }
                    // User cancelled the dialog
                }
            // Create the AlertDialog object and return it
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}