package com.example.chores.ui.show_task

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chores.R
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskList
import com.example.chores.data.model.TaskStatus
import com.example.chores.databinding.ShowTaskActivityBinding
import com.example.chores.ui.common.ConfirmFragment
import com.example.chores.ui.common.ConfirmationListener
import com.example.chores.ui.edit_task.EditTaskActivity
import kotlinx.android.synthetic.main.show_task_activity.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import layout.AfterTaskAdapter
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ShowTaskActivity : AppCompatActivity(), ConfirmationListener, MarkTaskFragment.SetStatusListener, KodeinAware {

    companion object {
        const val EDIT_TASK = 0
    }

    override val kodein by kodein()

    private val viewModelFactory: ShowTaskViewModelFactory by instance()

    lateinit var viewModel : ShowTaskViewModel

    override fun onConfirmation(confirmation: Boolean) {
        if (confirmation) {
            viewModel.removeTask()
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ShowTaskActivityBinding = DataBindingUtil.setContentView(this,R.layout.show_task_activity)
        viewModel = ViewModelProviders.of(this,viewModelFactory)[ShowTaskViewModel::class.java]
        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        setSupportActionBar(findViewById(R.id.toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Task details"

        val intent = intent
        val task = intent.getParcelableExtra<Task>("task")

        task?.run {
            viewModel.setTask(this)

            viewModel.getTaskLiveData(task.id).observe(this@ShowTaskActivity, Observer {
                if (it != null)
                {
                    viewModel.viewModelScope.launch {
                        // !! is ok, as the given id comes directly from the repo, so it can't not exist
                        viewModel.retrieveTaskWithEverything(it.id)?.run {
                            viewModel.setTask(this)
                        }
                    }
                }
            })

            val viewManager = LinearLayoutManager(this@ShowTaskActivity)
            val viewAdapter = AfterTaskAdapter(task.afterTasks ?: TaskList()).also {
                it.listener = object : AfterTaskAdapter.OnItemClickListener{
                    override fun onItemClick(v: View, position: Int) {
                        val taskId = it.afterTasks[position].id
                        viewModel.viewModelScope.launch {
                            @Suppress("NAME_SHADOWING")
                            val task = withContext(Dispatchers.Default) { viewModel.retrieveTaskWithEverything(taskId)}
                            Intent(this@ShowTaskActivity, ShowTaskActivity::class.java).run {
                                putExtra("task",task)
                                startActivity(this)
                            }
                        }
                    }
                }
            }

            viewModel.retrieveAfterTasksLiveData().observe(this@ShowTaskActivity, Observer { taskList ->
                viewAdapter.afterTasks = taskList
                viewAdapter.notifyDataSetChanged()
            })

            afterTaskRecyclerView.run {
                setHasFixedSize(true)

                layoutManager = viewManager
                adapter = viewAdapter
            }
        } ?: throw Exception("Task can't be null")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.task_menu,menu)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK)
        {
            when(requestCode) {
                EDIT_TASK -> {
                    if (data != null && data.extras != null)
                    {
                        val newTask = data.extras!!.getParcelable<Task>("result")
                        if (newTask != null)
                            viewModel.updateTask(newTask)
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId)
        {
            R.id.edit_task -> {
                viewModel.viewModelScope.launch {
                    val task = viewModel.retrieveTaskWithEverything(null)
                    Intent(this@ShowTaskActivity, EditTaskActivity::class.java).run {
                        putExtra("task",task)
                        startActivityForResult(this,
                            EDIT_TASK
                        )
                    }
                }
                return true
            }
            R.id.remove_task -> {
                ConfirmFragment(R.string.confirm_removal_title, R.string.confirm_removal_message).run {
                    show(supportFragmentManager,"Task removal confirmation")
                }
                return true
            }
            R.id.mark_task -> {
                val markTask = MarkTaskFragment()
                markTask.show(supportFragmentManager,"Task priority masking")
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    override fun onStatusSet(taskStatus: TaskStatus) {
        if (viewModel.status.value != taskStatus)
            viewModel.viewModelScope.launch {
                if (!viewModel.changeStatus(taskStatus))
                    withContext(Dispatchers.Main) {
                        Toast.makeText(this@ShowTaskActivity,getString(R.string.incomplete_aftertasks),Toast.LENGTH_SHORT).show()

                    }
            }
    }
}
