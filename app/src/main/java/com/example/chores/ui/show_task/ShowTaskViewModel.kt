package com.example.chores.ui.show_task

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.chores.data.model.*
import com.example.chores.data.repository.TaskRepository
import kotlinx.coroutines.launch
import java.util.Calendar

class ShowTaskViewModel(private val taskRepository: TaskRepository) : ViewModel() {

    var id = ""
    var title = MutableLiveData<String>()
    var description = MutableLiveData<String>()
    var priority= MutableLiveData<TaskPriority>()
    var className = MutableLiveData<String>()
    var datetime= MutableLiveData<Calendar>()
    var notificationString= MutableLiveData<String>()
    var status = MutableLiveData<TaskStatus>()

    suspend fun changeStatus(taskStatus: TaskStatus) : Boolean {
        if (taskStatus != TaskStatus.COMPLETED)
        {
            status.value = taskStatus
            taskRepository.setTaskStatus(id,taskStatus)
            return true
        }
        else
        {
            val task = taskRepository.retrieveTaskWithEverything(id)
            return task?.afterTasks?.let {
                it.forEach { task ->
                    if (task.status != TaskStatus.COMPLETED)
                        return@let false
                }
                status.value = taskStatus
                taskRepository.setTaskStatus(id,taskStatus)
                return@let true
            } ?: false
        }
    }

    fun updateTask(newTask: Task) {
        viewModelScope.launch {
            taskRepository.updateTask(newTask)
        }
    }

    fun setTask(task: Task) {
        id = task.id
        title.value = task.name
        description.value = task.description
        className.value = task.taskClass!!.name
        priority.value = task.priority
        datetime.value = task.datetime
        notificationString.value =
            "${task.notificationTime} " + if (task.notificationUnit == NotificationUnit.MINUTES) "min" else "hrs"
        status.value = task.status
    }

    fun removeTask() {
        viewModelScope.launch {
            taskRepository.removeTask(id)
        }
    }

    suspend fun retrieveTaskWithEverything(taskId: String?) : Task? {
        return if (taskId != null || id != "")
            taskRepository.retrieveTaskWithEverything(taskId ?: id)
        else
            null
    }

    fun retrieveAfterTasksLiveData(): LiveData<TaskList> {
        // Here the !! is justified as the task id is always set to the id of an existing task
        return taskRepository.retrieveAfterTasksLiveData(id)!!
    }

    fun getTaskLiveData(id: String): LiveData<Task> {
        return taskRepository.getTaskLiveData(id)
    }
}
