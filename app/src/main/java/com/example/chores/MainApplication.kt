package com.example.chores

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.example.chores.data.database.AppDatabase
import com.example.chores.data.database.ProdAppDatabaseFactory
import com.example.chores.data.database.TestingDatabase
import com.example.chores.data.repository.TaskRepository
import com.example.chores.data.repository.TaskRepositoryImpl
import com.example.chores.data.repository.TaskRepositoryMock
import com.example.chores.ui.delay_task.DelayTaskViewModelFactory
import com.example.chores.ui.edit_task.EditTaskViewModelFactory
import com.example.chores.ui.main.MainActivityViewModelFactory
import com.example.chores.ui.notifications.NotificationCenter
import com.example.chores.ui.notifications.NotificationCenterImpl
import com.example.chores.ui.notifications.NotificationCenterMock
import com.example.chores.ui.settings.SettingsValidator
import com.example.chores.ui.settings.SettingsValidatorImpl
import com.example.chores.ui.show_task.ShowTaskViewModelFactory
import com.example.chores.util.Constants
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.*

class MainApplication : Application(), KodeinAware {

    init {
        instance = this
    }

    override val kodein by Kodein.lazy {
        /* bindings */
        bind<AppDatabase>() with eagerSingleton { ProdAppDatabaseFactory.buildDatabase()}
        bind<TaskRepository>() with singleton { TaskRepositoryImpl() }
        bind() from provider { DelayTaskViewModelFactory(instance()) }
        bind() from provider { EditTaskViewModelFactory(instance()) }
        bind() from provider { MainActivityViewModelFactory(instance()) }
        bind() from provider { ShowTaskViewModelFactory(instance()) }
        bind<NotificationCenter>() with singleton { NotificationCenterImpl() }
        bind<SettingsValidator>() with singleton { SettingsValidatorImpl() }
    }

    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Chores"
            val descriptionText = "All notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(Constants.CHORES_NOTIFICATION_CHANNEL, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager=
                applicationContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}