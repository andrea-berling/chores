package com.example.chores.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskClass
import com.example.chores.data.model.TaskDependency

@Database(
    entities = [Task::class, TaskClass::class, TaskDependency::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun taskDao(): TaskDao
    abstract fun classDao(): ClassDao
    abstract fun dependencyDao(): DependencyDao
}