package com.example.chores.data.repository

import androidx.lifecycle.*
import com.example.chores.data.model.*
import java.util.*

interface TaskRepository {

    /**
     * Adds a new task to the repository and requests the NotificationCenter to register a notification for
     * the given task, according to the tasks's due datetime and notification time
     *
     * @param task task to add
     * @return true if the task has been successfully inserted, false otherwise
     */
    suspend fun addTask(task: Task) : Boolean

    /**
     * Updates a given task, based on its id and requests the NotificationCenter to update the nofitification for
     * the given task, according to the tasks's due datetime and notification time
     *
     * @param newTask task to update
     * @return true if the task has been successfully updated, false otherwise
     */
    suspend fun updateTask(newTask: Task) : Boolean

    /**
     * Removes a task with the given id and requests the NotificationCenter to remove the nofitification for
     * the given task, according to the tasks's due datetime and notification time
     *
     * @param id id of the task to remove
     * @return true if the task was successfully removed, false otherwise
     */
    suspend fun removeTask(id: String) : Boolean

    /**
     * Retrieves a task based on the given id, along with its class and after tasks
     *
     * @param id id of the task to retrieve
     * @return the requested tasks, if present. null otherwise
     */
    suspend fun retrieveTaskWithEverything(id: String): Task?

    /**
     * Retrieves the list of task classes present in the repository, as a liveData
     *
     * This list is never empty, as at least the default class is always present
     *
     * @return a live data containing the list of all classes
     */
    fun retrieveClassesLiveData() : LiveData<List<TaskClass>>

    /**
     * Retrieves the list of all tasks as a live data
     *
     * @return a live data containing the list of all tasks
     */
    fun retrieveAllTasksLiveData() : LiveData<TaskList>

    /**
     * Retrieves the list of all future tasks (from the instant this function is called), if id is null
     *
     * Otherwise, retrieves list of future tasks that don't depend of the task with the given id
     *
     * A task t0 depends on a task t1 if t1 needs to be done before t0 (i.e. t1 is in the after tasks list of t0)
     *
     * @param id id of the task on which the returned task must not depend
     * @return a live data containing the list of all tasks
     */
    suspend fun retrieveFutureTasks(id : String?) : TaskList

    /**
     * Retrieves a fresh id of the specified type
     *
     * @param type type of the Id to return. One of FreshId.TASK or FreshId.CLASS
     * @return a fresh id
     */
    suspend fun getFreshId(type : FreshId) : String

    /**
     * Changes the status of a task with the given id, according to the given Status
     * If the given status is not COMPLETED, and there are tasks that depend on the task with the
     * given id with a COMPLETED status, their status is recursively set to PENDING
     *
     * @param id id of the task
     * @param taskStatus new status
     * @return true if the status was correctly set, false otherwise
     */
    suspend fun setTaskStatus(id: String, taskStatus: TaskStatus) : Boolean

    /**
     * Retrieves the class with the given id, if present
     *
     * @param id id of the task to retrieve
     * @return the requested class, if present; null otherwise
     */
    suspend fun retrieveClass(id: String): TaskClass?

    /**
     * Adds a new class with the given name and priority
     *
     * @param name name of the class to add
     * @param priority priority of the class to add
     */
    suspend fun addClass(name: String, priority: ClassPriority)

    /**
     * Updates a given class, based on its id
     *
     * @param taskClass class to update
     * @return true if the class has been successfully updated, false otherwise
     */
    suspend fun updateClass(taskClass: TaskClass) : Boolean

    /**
     * Removes the class with the given id, if present
     *
     * @param id id of the class to remove
     * @return true if the removal was successful; false otherwise
     */
    suspend fun removeClass(id: String) : Boolean

    /**
     * Retrieves the list of task classes present in the repository
     *
     * @return the list of all classes
     */
    suspend fun retrieveClasses(): List<TaskClass>

    /**
     * Retrieves the list of task of the class with the given id, if present, as a live data
     *
     * @param classId id of the class
     * @return the list of tasks of the given class, as a live data, if present; null otherwise
     */
    fun retrieveTasksOfClassLiveData(classId: String): LiveData<TaskList>?

    /**
     * Retrieves the tasks of which the task with the given id depends, as a liveData, if present
     *
     * @param taskId id of the task
     * @return the list of tasks on which the task depends, if present; null otherwise
     */
    fun retrieveAfterTasksLiveData(taskId: String): LiveData<TaskList>?

    /**
     * Retrieves the number of tasks of the class with the given id, if present
     *
     * @param classId the id of the class
     * @return the number of tasks of the given class, if present; -1 otherwise
     */
    suspend fun retrieveTaskNumberOfClass(classId: String) : Int

    /**
     * Retrieves the list of tasks that have a due datetime that falls in the given range
     *
     * @param start range start
     * @param end range end
     * @return the list of tasks that have a due datetime that falls in the given range
     */
    suspend fun retrieveTasksOfRange(start: Calendar, end: Calendar): TaskList

    /**
     * Retrieves the list of future tasks as a live data
     *
     * If a calendar is given, the tasks with a due datetime after its date are returned
     * @param datetime calendar for future tasks
     * @return the list of future tasks, as a live data
     */
    fun retrieveFutureTasksLiveData(datetime: Calendar?): LiveData<TaskList>

    /**
     * Retrieves a LiveData for the task with the requested id
     *
     * @param id id of the task to retrieve
     * @return LiveData for the requested task
     */
    fun getTaskLiveData(id: String): LiveData<Task>
}

enum class FreshId {
    TASK, CLASS
}
