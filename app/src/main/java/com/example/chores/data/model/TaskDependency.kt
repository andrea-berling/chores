package com.example.chores.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index

@Entity(primaryKeys = ["waiting_task","waiting_on"],
        foreignKeys = [ForeignKey(entity = Task::class,parentColumns = ["id"],childColumns = ["waiting_task"],onDelete = CASCADE),
        ForeignKey(entity = Task::class,parentColumns = ["id"],childColumns = ["waiting_on"],onDelete = CASCADE)],
        indices = [Index(value = ["waiting_task"]), Index(value=["waiting_on"])],
        tableName = "task_dependencies"
)
data class TaskDependency (
    @ColumnInfo(name = "waiting_task") var waitingTask:String,
    @ColumnInfo(name = "waiting_on") var waitingOn:String
)