package com.example.chores.data.model

import android.os.Parcel
import android.os.Parcelable

typealias TaskFilter = (Task) -> Boolean

class TaskList() : ArrayList<Task>(),Parcelable {
    constructor(parcel: Parcel) : this() {
        val n = parcel.readInt()
        for(i in 1..n)
            this.add(parcel.readParcelable(Task::class.java.classLoader)!!)
    }

    constructor(taskList: List<Task>) : this() {
        this.addAll(taskList)
    }

    fun of(vararg tasks : Task) : TaskList {
        this.addAll(tasks)
        return this
    }

    fun indexOfId(id:String) : Int {
        for ((i, t) in this.withIndex())
        {
            if (t.id == id)
                return i
        }
        return -1
    }

    fun filter(filter: TaskFilter): TaskList {
        val result = TaskList()
        for(t in this)
            if (filter(t))
                result.add(t)
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(this.size)
        for(task in this)
            parcel.writeParcelable(task,flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskList> {
        override fun createFromParcel(parcel: Parcel): TaskList {
            return TaskList(parcel)
        }

        override fun newArray(size: Int): Array<TaskList?> {
            return arrayOfNulls(size)
        }
    }
}