package com.example.chores.data.model

import android.os.Parcel
import android.os.Parcelable
import android.provider.SyncStateContract
import androidx.room.*
import com.example.chores.util.Constants
import java.util.*

@Entity(
    tableName = "tasks",
    foreignKeys = [ForeignKey(entity= TaskClass::class, parentColumns = ["id"], childColumns = ["task_class_id"], onDelete = ForeignKey.CASCADE)],
    indices = [Index(value = ["task_class_id"])]
)
data class Task(
    @PrimaryKey var id: String,
    var name: String,
    var description: String,
    var priority: TaskPriority,
    @ColumnInfo(name = "task_class_id") var taskClassId: String,
    var datetime: Calendar,
    @ColumnInfo(name = "notification_time") var notificationTime: Int,
    @ColumnInfo(name = "notification_unit") var notificationUnit: NotificationUnit,
    var status: TaskStatus,
    @Ignore var taskClass: TaskClass?,
    @Ignore var afterTasks: TaskList?
) : Parcelable {

    @Ignore
    constructor(task : Task) : this (
        task.id,
        task.name,
        task.description,
        task.priority,
        task.taskClassId,
        task.datetime,
        task.notificationTime,
        task.notificationUnit,
        task.status,
        task.taskClass,
        task.afterTasks
    )

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        TaskPriority.values()[parcel.readInt()],
        parcel.readString()!!,
        Calendar.getInstance().apply { timeInMillis = parcel.readLong()},
        parcel.readInt(),
        NotificationUnit.values()[parcel.readInt()],
        TaskStatus.values()[parcel.readInt()],
        parcel.readParcelable(TaskClass::class.java.classLoader),
        parcel.readParcelable(TaskList::class.java.classLoader)
    )

    constructor (
        id: String,
        name: String,
        description: String,
        priority: TaskPriority,
        taskClassId: String,
        datetime: Calendar,
        notificationTime: Int,
        notificationUnit: NotificationUnit,
        status: TaskStatus
    ) : this(
        id,
        name,
        description,
        priority,
        taskClassId,
        datetime,
        notificationTime,
        notificationUnit,
        status,
        null,
        null
     )

    @Ignore
    constructor(
        id : String,
        name: String,
        taskClassId: String
    ) : this(
        id,
        name,
        "",
        TaskPriority.NEUTRAL,
        taskClassId,
        Calendar.getInstance(),
        0,
        NotificationUnit.MINUTES,
        TaskStatus.PENDING
    )

    @Ignore
    constructor(
        id : String,
        name: String,
        taskClassId: String,
        datetime: Calendar,
        status: TaskStatus
    ) : this(
        id,
        name,
        "",
        TaskPriority.NEUTRAL,
        taskClassId,
        datetime,
        0,
        NotificationUnit.MINUTES,
        status
    )

    @Ignore
    constructor(
        id : String,
        name: String,
        priority: TaskPriority,
        taskClassId: String,
        datetime: Calendar
    ) : this(
        id,
        name,
        "",
        priority,
        taskClassId,
        datetime,
        0,
        NotificationUnit.MINUTES,
        TaskStatus.PENDING
    )

    @Ignore
    constructor(
        id : String,
        priority: TaskPriority,
        status: TaskStatus
    ) : this(
        id,
        "task$id",
        "",
        priority,
        Constants.DEFAULT_CLASS_ID,
        Calendar.getInstance(),
        0,
        NotificationUnit.MINUTES,
        status
    )

    fun getNotificationOffsetInMillis(): Long {
        return when (notificationUnit) {
            NotificationUnit.MINUTES -> notificationTime*60*1000L
            NotificationUnit.HOURS -> notificationTime*3600*1000L
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeInt(priority.ordinal)
        parcel.writeString(taskClassId)
        parcel.writeLong(datetime.timeInMillis)
        parcel.writeInt(notificationTime)
        parcel.writeInt(notificationUnit.ordinal)
        parcel.writeInt(status.ordinal)
        parcel.writeParcelable(taskClass, flags)
        parcel.writeParcelable(afterTasks, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Task> {
        override fun createFromParcel(parcel: Parcel): Task {
            return Task(parcel)
        }

        override fun newArray(size: Int): Array<Task?> {
            return arrayOfNulls(size)
        }
    }

}

enum class NotificationUnit {
    MINUTES,HOURS
}

enum class TaskStatus {
    PENDING, ONGOING, COMPLETED;

    override fun toString(): String {
        return when (this) {
            PENDING -> "Pending"
            ONGOING -> "Ongoing"
            COMPLETED -> "Completed"
        }
    }
}

enum class TaskPriority {
    LOW, NEUTRAL, HIGH, VERY_HIGH;

    override fun toString(): String {
        return when (this) {
            LOW -> "Low"
            NEUTRAL -> "Neutral"
            HIGH -> "High"
            VERY_HIGH -> "Very High"
        }
    }
}