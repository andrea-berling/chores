package com.example.chores.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.chores.MainApplication
import com.example.chores.data.model.*
import com.example.chores.ui.notifications.NotificationCenter
import com.example.chores.ui.settings.SettingsValidator
import com.example.chores.util.Constants
import com.example.chores.util.Utils
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.lang.Exception
import java.util.*
import kotlin.collections.HashMap

class TaskRepositoryMock : TaskRepository,KodeinAware {

    override fun getTaskLiveData(id: String): LiveData<Task> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    val tasks = HashMap<String,Task>().apply {
        //DataSets.loadPieChartTasksDataset(this)
        //DataSets.loadHistogramTasksDataset(this)
        //DataSets.loadLineChartTasksDataset(this)
        //DataSets.loadNotificationFilterTasksDataSet(this)
        DataSets.loadTaskFiltersTasksDataSet(this)
    }

    val classes = HashMap<String,TaskClass>().apply {
        //DataSets.loadPieChartClassesDataset(this)
        //DataSets.loadHistogramClassesDataset(this)
        DataSets.loadNotificationFilterClassesDataSet(this)
    }

    override val kodein: Kodein by kodein(MainApplication.applicationContext())
    private val tasksLiveData = MutableLiveData<TaskList>()
    private val classesLiveData = MutableLiveData<List<TaskClass>>()
    private val notificationCenter : NotificationCenter by instance()
    private val settingsValidator : SettingsValidator by instance()

    override suspend fun addTask(task: Task): Boolean {
        tasks[task.id] = task
        tasksLiveData.value = TaskList(tasks.values.toList())
        if(settingsValidator.shouldNotify(task))
            notificationCenter.addNotification(task)
        return true
    }

    override suspend fun updateTask(newTask: Task): Boolean {
        return addTask(newTask)
    }

    override suspend fun removeTask(id: String): Boolean {
        val task = tasks[id]
        return if(task != null) {
            tasks.remove(task.id)
            tasksLiveData.value = TaskList(tasks.values.toList())
            if(notificationCenter.isNotificationSet(task))
                notificationCenter.removeNotification(task)
            true
        } else
            false
    }

    override suspend fun retrieveTaskWithEverything(id: String): Task? {
        return tasks[id]?.let {
            Task(it).apply {
                // Not quite everything, after tasks are misssing. Shouldn't be a problem though
                taskClass = classes[it.taskClassId]
            }
        }
    }

    override fun retrieveClassesLiveData(): LiveData<List<TaskClass>> {
        if (classesLiveData.value == null)
        {
            classesLiveData.postValue(classes.values.toList())
        }
        return classesLiveData
    }

    override fun retrieveAllTasksLiveData(): LiveData<TaskList> {
        if (tasksLiveData.value == null) {
            tasksLiveData.postValue(TaskList(tasks.values.toList()))
        }
        return tasksLiveData
    }

    override suspend fun retrieveFutureTasks(id: String?): TaskList {
        val futureTasks = TaskList()
        val now = Calendar.getInstance()
        for(task in tasks.values)
            if (task.datetime >= now)
                futureTasks.add(task)
        id?.run {
            val idsToExclude = mutableListOf(this)
            for((taskId,task) in tasks)
                task.afterTasks?.run {
                    if(contains(task))
                        idsToExclude.add(taskId)
                }
            return futureTasks.filter {
                !idsToExclude.contains(it.id)
            }
        } ?: return futureTasks
    }

    override suspend fun getFreshId(type: FreshId): String {
        val usedIds = when(type) {
            FreshId.TASK -> tasks.keys.toList()
            FreshId.CLASS -> classes.keys.toList()
        }

        return Utils.randomIdHelper(usedIds)
    }

    override suspend fun setTaskStatus(id: String, taskStatus: TaskStatus): Boolean {
        return tasks[id]?.let {
            it.status = taskStatus
            tasksLiveData.value = TaskList(tasks.values.toList())
            true
        } ?: false
    }

    override suspend fun retrieveClass(id: String): TaskClass? {
        return classes[id]
    }

    override suspend fun addClass(name: String, priority: ClassPriority) {
        val id = Utils.randomIdHelper(classes.keys.toList())
        if (!classes.containsKey(id))
        {
            classes[id] = TaskClass(id,name,priority)
            classesLiveData.value = classes.values.toList()
        }
    }

    override suspend fun retrieveClasses(): List<TaskClass> {
        return classes.values.toList()
    }

    override fun retrieveTasksOfClassLiveData(classId: String): LiveData<TaskList> {
        return Transformations.map(retrieveAllTasksLiveData()) { taskList ->
            return@map taskList.filter {
                it.taskClassId == classId
            }
        }
    }

    override suspend fun removeClass(id: String): Boolean {
        return if(classes.containsKey(id)) {
            classes.remove(id)
            classesLiveData.value = classes.values.toList()
            true
        }
        else false
    }

    override fun retrieveAfterTasksLiveData(taskId: String): LiveData<TaskList> {
        val masterTask = tasks[taskId]
        return masterTask?.let {
            return@let Transformations.map(retrieveAllTasksLiveData()) {
                return@map it.filter { task ->
                    masterTask.afterTasks?.contains(task) ?: false
                }
            }
        } ?: throw Exception("Task not found")
    }

    override suspend fun retrieveTaskNumberOfClass(classId: String): Int {
        var n = 0
        for (task in tasks.values)
            if (task.taskClassId == classId)
                n++
        return n
    }

    override suspend fun retrieveTasksOfRange(start: Calendar, end: Calendar): TaskList {
        val allTasks = TaskList(tasks.values.toList())
        return allTasks.filter {task ->
            task.datetime in start..end
        }
    }

    override fun retrieveFutureTasksLiveData(datetime: Calendar?): LiveData<TaskList> {
        return Transformations.map(retrieveAllTasksLiveData()) {
            val c = datetime ?: Calendar.getInstance()
            return@map it.filter { task ->
                task.datetime >= c
            }
        }
    }

    override suspend fun updateClass(taskClass: TaskClass): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

object DataSets {

    val loadTaskFiltersTasksDataSet : HashMap<String,Task>.() -> Unit = {
        for(i in 1..7)
            for(status in TaskStatus.values())
                for(priority in TaskPriority.values())
                {
                    val id = "${pad(i)}${pad(status.ordinal)}${pad(priority.ordinal)}"
                    put(id,Task(id,priority,status))
                }
    }

    val loadNotificationFilterTasksDataSet : HashMap<String,Task>.() -> Unit = {
        val c = Calendar.getInstance().apply {
            add(Calendar.DAY_OF_MONTH,5)
        }

        for(priority in TaskPriority.values())
            for(j in 1..3)
                for(i in 1..7)
                {
                    "${pad(priority.ordinal)}${pad(i)}".run {
                        put(this,Task(this,"task$this",priority, pad(j),c))
                    }
                }
    }

    val loadNotificationFilterClassesDataSet : HashMap<String,TaskClass>.() -> Unit = {
        put(Constants.DEFAULT_CLASS_ID, TaskClass(Constants.DEFAULT_CLASS_ID,"Default",ClassPriority.NEUTRAL))
        for(priority in ClassPriority.values())
        {
            val id = pad(priority.ordinal)
            put(id,TaskClass(id,"Class$id",priority))
        }
    }

    val loadPieChartTasksDataset : HashMap<String,Task>.() -> Unit = {
        // Pie Chart Testing Dataset
        val valuesPerClass = listOf(20,50,60,15,90,5,2)
        for((j,v) in valuesPerClass.withIndex()) {
            for(i in 1..v)
            {
                val id = "${pad(j)}${pad(i)}"
                put(id,Task(id,"task$id",pad(j)))
            }
        }
    }

    val loadPieChartClassesDataset : HashMap<String,TaskClass>.() -> Unit  = {
        // Pie Chart Testing Dataset
        val n = 6
        for(i in 0..n) {
            val id = pad(i)
            put(id,TaskClass(id,"Class$i",TaskPriority.NEUTRAL))
        }
    }

    val loadHistogramTasksDataset : HashMap<String,Task>.() -> Unit = {
        // Histogram Test Dataset
        val start = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
        }

        val taskClassId = "000"

        val triples = listOf(
            listOf(10,3,7),
            listOf(7,4,15),
            listOf(5,9,14),
            listOf(8,9,8),
            listOf(14,10,21),
            listOf(6,9,8),
            listOf(10,20,15)
        )

        for((i,triple) in triples.withIndex())
        {
            for(status in TaskStatus.values())
                for(j in 1..triple[status.ordinal])
                {
                    val id = "${pad(i)}${status.ordinal}${pad(j)}"
                    val c = Calendar.getInstance().apply {
                        timeInMillis = start.timeInMillis
                        add(Calendar.DAY_OF_YEAR,i)
                    }
                    put(id,Task(id,"task$id",taskClassId,c,status))
                }
        }
    }

    val loadHistogramClassesDataset : HashMap<String,TaskClass>.() -> Unit = {
        // Histogram Testing Dataset
        val id = "000"
        put(id,TaskClass(id,"Class$id",TaskPriority.NEUTRAL))
    }

    val loadLineChartTasksDataset : HashMap<String,Task>.() -> Unit = {
        val start = Calendar.getInstance().apply {
            set(Calendar.MILLISECOND,0)
            set(Calendar.HOUR_OF_DAY,0)
            set(Calendar.MINUTE,0)
            set(Calendar.SECOND,0)
            set(Calendar.DAY_OF_WEEK,0)
            add(Calendar.WEEK_OF_YEAR,-4)
        }

        val values = arrayOf(5,6,4,5,15,16,9,20)

        for ((i,value) in values.withIndex())
        {
            val c = Calendar.getInstance().apply {
                timeInMillis = start.timeInMillis
                add(Calendar.WEEK_OF_YEAR,i)
            }
            for(j in 1..value) {
                val id = "${pad(i)}000${pad(j)}"
                put(id,Task(id,"task$id","000",c,TaskStatus.COMPLETED))
            }
        }
    }

    private fun pad(i : Int) : String {
        return if (i >= 10) i.toString()
        else "0$i"
    }

}