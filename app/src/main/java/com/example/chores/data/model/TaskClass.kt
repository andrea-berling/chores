package com.example.chores.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

typealias ClassPriority = TaskPriority

@Entity(
    tableName = "task_class"
)
data class TaskClass(
    @PrimaryKey var id: String,
    var name: String,
    var priority: ClassPriority,
    @Ignore var tasks : TaskList?
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        ClassPriority.values()[parcel.readInt()],
        parcel.readParcelable(TaskList::class.java.classLoader)
    )

    constructor (
        id : String,
        name : String,
        priority : ClassPriority
    ) : this (
        id,
        name,
        priority,
        null
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeInt(priority.ordinal)
        parcel.writeParcelable(tasks, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskClass> {
        override fun createFromParcel(parcel: Parcel): TaskClass {
            return TaskClass(parcel)
        }

        override fun newArray(size: Int): Array<TaskClass?> {
            return arrayOfNulls(size)
        }
    }
}
