package com.example.chores.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.chores.data.model.TaskClass
import com.example.chores.util.Constants

@Dao
interface ClassDao {

    @Insert
    suspend fun insert(task_class: TaskClass)

    @Delete
    suspend fun delete(task_class: TaskClass)

    @Update
    suspend fun update(task_class: TaskClass)

    @Query("select * from task_class")
    fun getAllClassesLiveData() : LiveData<List<TaskClass>>

    @Query("select * from task_class")
    suspend fun getClasses() : List<TaskClass>

    @Query("select * from task_class where id = :id")
    suspend fun getClass(id:String): TaskClass

    @Query("select id from task_class")
    suspend fun getAllIds(): List<String>

    @Query("select id from task_class")
    suspend fun getAllIdsRaw(): List<String>

    @Query("select * from task_class where id='${Constants.DEFAULT_CLASS_ID}'")
    suspend fun getDefaultClass(): TaskClass

    @Query("delete from task_class where id=:id")
    fun deleteClassWithId(id: String)
}
