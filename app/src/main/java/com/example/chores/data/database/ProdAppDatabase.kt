package com.example.chores.data.database

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.chores.MainApplication
import com.example.chores.R
import com.example.chores.data.model.ClassPriority
import com.example.chores.data.model.TaskClass
import com.example.chores.data.model.TaskPriority
import org.xmlpull.v1.XmlPullParser
import java.lang.Exception

object ProdAppDatabaseFactory {

    fun buildDatabase() = Room.databaseBuilder(
        MainApplication.applicationContext(),
        AppDatabase::class.java, "chores-database"
    ).addCallback(object: RoomDatabase.Callback(){
        /* Populate initial database */
        override fun onCreate(db : SupportSQLiteDatabase) {
            super.onCreate(db)
            addDefaultClassesFromXml(db)
        }

        private fun addDefaultClassesFromXml(db: SupportSQLiteDatabase) {
            val classes = ArrayList<TaskClass>()
            val xml = MainApplication.applicationContext().resources.getXml(R.xml.default_classes)
            try {
                var eventType = xml.eventType
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG && xml.name == "class") {
                        lateinit var id: String
                        lateinit var priority: TaskPriority
                        lateinit var name: String
                        while (xml.name != "class" || xml.eventType != XmlPullParser.END_TAG) {
                            when (xml.name) {
                                "id" -> {
                                    xml.next()
                                    id = xml.text
                                    xml.next()
                                }

                                "name" -> {
                                    xml.next()
                                    name = xml.text
                                    xml.next()
                                }

                                "priority" -> {
                                    xml.next()
                                    priority = ClassPriority.valueOf(xml.text)
                                    xml.next()
                                }
                            }
                            xml.next()
                        }
                        classes.add(TaskClass(id, name, priority))
                    }
                    eventType = xml.next()
                }
            } catch (e: Exception) {
                Log.e("XmlPullParserException", e.toString())
            }
            xml.close()
            for (c in classes) {
                val cv = ContentValues()
                cv.put("id", c.id)
                cv.put("name", c.name)
                cv.put("priority", c.priority.ordinal)
                db.insert("task_class", SQLiteDatabase.CONFLICT_NONE, cv)
            }
        }
    }).build()
}