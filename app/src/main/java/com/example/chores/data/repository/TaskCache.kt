package com.example.chores.data.repository

import com.example.chores.data.model.Task

class TaskCache {
    private val tasks : HashMap<String, Task> = HashMap()

    fun getTask(id: String) : Task? {
        return tasks[id]
    }

    fun putTask(t: Task) {
        tasks[t.id] = t
    }

    fun clear() {
        tasks.clear()
    }

    fun remove(id: String) {
        tasks.remove(id)
    }
}