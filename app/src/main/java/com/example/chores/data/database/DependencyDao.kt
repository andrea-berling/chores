package com.example.chores.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.chores.data.model.TaskDependency

@Dao
interface DependencyDao {
    @Insert
    suspend fun insert(taskDependency: TaskDependency)
    @Delete
    suspend fun delete(taskDependency: TaskDependency)
    @Update
    suspend fun update(taskDependency: TaskDependency)

    @Query("select * from task_dependencies where waiting_task = :id")
    fun getDependenciesLiveData(id : String) : LiveData<List<TaskDependency>>

    @Query("select * from task_dependencies where waiting_task = :id")
    suspend fun getDependencies(id : String) : List<TaskDependency>

    @Query("delete from task_dependencies where waiting_task = :id")
    suspend fun deleteAllDependencies(id: String)

    @Query("select * from task_dependencies where waiting_on = :id")
    suspend fun getInverseDependencies(id: String): List<TaskDependency>
}
