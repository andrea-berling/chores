package com.example.chores.data.database

import androidx.room.TypeConverter
import com.example.chores.data.model.NotificationUnit
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.model.TaskStatus
import java.util.*

class Converters {
    @TypeConverter
    fun fromPriorityToInt(taskPriority: TaskPriority?) : Int? = taskPriority?.ordinal

    @TypeConverter
    fun fromIntToPriority(int : Int?) : TaskPriority? = int?.let{ TaskPriority.values()[int]}

    @TypeConverter
    fun fromCalendarToLong(calendar: Calendar?) : Long? = calendar?.timeInMillis

    @TypeConverter
    fun fromLongToCalendar(long: Long?) : Calendar? = long?.let {Calendar.getInstance().apply { timeInMillis = long}}

    @TypeConverter
    fun fromStatusToInt(taskStatus: TaskStatus?) : Int? = taskStatus?.ordinal

    @TypeConverter
    fun fromIntToStatus(int : Int?) : TaskStatus? = int?.let{ TaskStatus.values()[int]}

    @TypeConverter
    fun fromNotificationUnitToInt(notificationUnit: NotificationUnit?) : Int? = notificationUnit?.ordinal

    @TypeConverter
    fun fromIntToNotificationUnit(int: Int?) : NotificationUnit? = int?.let{ NotificationUnit.values()[int] }
}