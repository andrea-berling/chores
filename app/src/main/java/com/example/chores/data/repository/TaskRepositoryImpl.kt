package com.example.chores.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.chores.MainApplication
import com.example.chores.data.database.AppDatabase
import com.example.chores.data.model.*
import com.example.chores.ui.notifications.NotificationCenter
import com.example.chores.ui.settings.SettingsValidator
import com.example.chores.util.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class TaskRepositoryImpl : KodeinAware,TaskRepository {

    override val kodein by kodein(MainApplication.applicationContext())

    private val db : AppDatabase by instance()
    private val notificationCenter : NotificationCenter by instance()
    private val settingsValidator : SettingsValidator by instance()


    override suspend fun addTask(task: Task) : Boolean
    {
        return withContext(Dispatchers.IO) {
            db.taskDao().insert(task)
            task.afterTasks?.forEach { dependee ->
                db.dependencyDao().insert(
                    TaskDependency(
                        task.id,
                        dependee.id
                    )
                )
            }
            if(settingsValidator.shouldNotify(task))
                notificationCenter.addNotification(task)
            return@withContext true
        }
    }

    override suspend fun updateTask(newTask: Task) : Boolean {
        return withContext(Dispatchers.IO) {
            db.taskDao().update(newTask)
            db.dependencyDao().deleteAllDependencies(newTask.id)
            newTask.afterTasks?.forEach {
                db.dependencyDao().insert(
                    TaskDependency(
                        newTask.id,
                        it.id
                    )
                )
            }
            if(settingsValidator.shouldNotify(newTask))
                notificationCenter.addNotification(newTask)
            return@withContext true
        }
    }

    override suspend fun removeTask(id: String) : Boolean {
        return withContext(Dispatchers.IO) {
            val task = retrieveTaskWithEverything(id)
            db.taskDao().deleteTaskWithId(id)
            db.dependencyDao().deleteAllDependencies(id)
            if (notificationCenter.isNotificationSet(task))
                notificationCenter.removeNotification(task)
            return@withContext true
        }
    }

    override suspend fun retrieveTaskWithEverything(id: String): Task {
        return withContext(Dispatchers.IO) {
            val task = db.taskDao().getTaskWithId(id)
            task.taskClass = db.classDao().getClass(task.taskClassId)
            task.afterTasks = TaskList(db.taskDao().getAfterTasks(id))
            return@withContext task
        }
    }


    override fun retrieveClassesLiveData() : LiveData<List<TaskClass>> {
        return db.classDao().getAllClassesLiveData()
    }

    override fun retrieveAllTasksLiveData() : LiveData<TaskList> {
        return Transformations.map(db.taskDao().getAllTasksLiveData()) {
            return@map TaskList(it)
        }
    }

    override suspend fun retrieveFutureTasks(id : String?) : TaskList {
        return withContext(Dispatchers.IO) {
            val futureTasks =  TaskList(db.taskDao().getFutureTasks(Calendar.getInstance().timeInMillis))
            if (id != null)
            {
                val dependencies = db.dependencyDao().getInverseDependencies(id)
                val idsToExclude = mutableListOf(id)
                idsToExclude.addAll(idsToExclude.size,dependencies.map{it.waitingTask})
                return@withContext futureTasks.filter { task ->
                    !idsToExclude.contains(task.id)
                }
            }
            else
                return@withContext futureTasks
        }
    }

    override suspend fun getFreshId(type : FreshId) : String {

        val usedIds = when(type) {
            FreshId.TASK -> db.taskDao().getAllIds()
            FreshId.CLASS -> db.classDao().getAllIds()
        }

        return Utils.randomIdHelper(usedIds)
    }

    override suspend fun setTaskStatus(id: String, taskStatus: TaskStatus): Boolean {
        return withContext(Dispatchers.IO) {
            db.taskDao().setTaskStatus(id, taskStatus.ordinal)
            if (taskStatus != TaskStatus.COMPLETED)
            {
                val taskDependencies = db.dependencyDao().getInverseDependencies(id).map { it.waitingTask }
                taskDependencies.forEach{
                    val task = db.taskDao().getTaskWithId(it)
                    if (task.status == TaskStatus.COMPLETED)
                        setTaskStatus(it,TaskStatus.PENDING)
                }
            }
            return@withContext true
        }
    }

    override suspend fun retrieveClass(id: String): TaskClass {
        return db.classDao().getClass(id)
    }

    override suspend fun addClass(name: String, priority: ClassPriority) {
        withContext(Dispatchers.IO) {
            val id =
                Utils.randomIdHelper(db.classDao().getAllIdsRaw())
            db.classDao().insert(TaskClass(id, name, priority))
        }
    }

    override suspend fun retrieveClasses(): List<TaskClass> {
        return withContext(Dispatchers.IO) {
            return@withContext db.classDao().getClasses()
        }
    }

    override fun retrieveTasksOfClassLiveData(classId: String): LiveData<TaskList> {
        return Transformations.map(db.taskDao().getTasksOfClassLiveData(classId)) {
            return@map TaskList(it)
        }
    }

    override suspend fun removeClass(id: String): Boolean {
        return withContext(Dispatchers.IO) {
            db.classDao().deleteClassWithId(id)
            return@withContext true
        }
    }

    override fun retrieveAfterTasksLiveData(taskId: String): LiveData<TaskList> {
        return Transformations.map(db.taskDao().getAfterTasksLiveData(taskId)) {
            return@map TaskList(it)
        }
    }

    override suspend fun retrieveTaskNumberOfClass(classId: String): Int {
        return withContext(Dispatchers.IO) {
            return@withContext db.taskDao().getNumberOfTasksWithClass(classId)
        }
    }

    override suspend fun retrieveTasksOfRange(start: Calendar, end: Calendar): TaskList {
        return withContext(Dispatchers.IO) {
            return@withContext TaskList(db.taskDao().getTasksOfRange(start.timeInMillis,end.timeInMillis))
        }
    }

    override fun retrieveFutureTasksLiveData(datetime: Calendar?): LiveData<TaskList> {
        val c = datetime ?: Calendar.getInstance()
        return Transformations.map(db.taskDao().getFutureTasksLiveData(c.timeInMillis)) {
            return@map TaskList(it)
        }
    }

    override suspend fun updateClass(taskClass: TaskClass): Boolean {
        return withContext(Dispatchers.IO) {
            db.classDao().update(taskClass)
            return@withContext true
        }
    }

    override fun getTaskLiveData(id: String): LiveData<Task> {
        return db.taskDao().getTaskWithIdLiveData(id)
    }
}

