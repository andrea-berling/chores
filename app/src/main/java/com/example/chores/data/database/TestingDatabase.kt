package com.example.chores.data.database

import androidx.room.Room
import com.example.chores.MainApplication
import com.example.chores.data.model.TaskClass
import com.example.chores.data.model.TaskPriority
import com.example.chores.util.Constants
import kotlinx.coroutines.runBlocking

object TestingDatabase {

    fun buildDatabase() : AppDatabase = Room.inMemoryDatabaseBuilder(MainApplication.applicationContext(), AppDatabase::class.java).build().apply {
        runBlocking {
            this@apply.classDao().insert(
                TaskClass(
                    Constants.DEFAULT_CLASS_ID,"Default",
                    TaskPriority.NEUTRAL)
            )
        }
    }

}