package com.example.chores.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.chores.data.model.Task
import java.util.*

@Dao
interface TaskDao {
    @Insert
    suspend fun insert(vararg task: Task)

    @Update
    suspend fun update(task: Task)

    @Delete
    suspend fun delete(vararg task: Task)

    @Query("select * from tasks")
    fun getAllTasksLiveData(): LiveData<List<Task>>

    @Query("select * from tasks")
    suspend fun getAllTasks(): List<Task>

    @Query("select * from tasks where datetime >= :datetime")
    fun getFutureTasksLiveData(datetime: Long): LiveData<List<Task>>

    @Query("select * from tasks where datetime >= :datetime")
    suspend fun getFutureTasks(datetime: Long): List<Task>

    @Query("select * from tasks where task_class_id = :id")
    fun getTasksOfClassLiveData(id: String): LiveData<List<Task>>

    @Query("select id from tasks")
    suspend fun getAllIds(): List<String>

    @Query("delete from tasks where id = :taskId")
    suspend fun deleteTaskWithId(taskId: String)

    @Query("select * from tasks where id = :taskId")
    fun getTaskWithId(taskId: String) : Task

    @Query("select * from tasks where id = :taskId")
    fun getTaskWithIdLiveData(taskId: String): LiveData<Task>

    @Query("update tasks set status = :taskStatus where id = :id")
    suspend fun setTaskStatus(id: String, taskStatus: Int)

    @Query("select T2.* from tasks as T1 inner join task_dependencies as TD on T1.id = TD.waiting_task inner join tasks as T2 on T2.id = TD.waiting_on where T1.id = :taskId")
    suspend fun getAfterTasks(taskId: String) : List<Task>

    @Query("select T2.* from tasks as T1 inner join task_dependencies as TD on T1.id = TD.waiting_task inner join tasks as T2 on T2.id = TD.waiting_on where T1.id = :taskId")
    fun getAfterTasksLiveData(taskId: String) : LiveData<List<Task>>

    @Query("select count(*) from tasks where task_class_id = :id")
    suspend fun getNumberOfTasksWithClass(id: String) : Int

    @Query("select * from tasks where datetime >= :start and datetime <= :end")
    suspend fun getTasksOfRange(start: Long, end: Long): List<Task>
}
