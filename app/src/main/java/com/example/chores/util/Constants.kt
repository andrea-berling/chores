package com.example.chores.util

import com.example.chores.MainApplication

object Constants {
    const val DEFAULT_CLASS_ID = "000000000000000000000000"
    const val CHORES_NOTIFICATION_CHANNEL = "Chores"
    private val PACKAGE_NAME: String = MainApplication.applicationContext().packageName
    val ACTION_MARK_ONGOING = "$PACKAGE_NAME.MARKONGOING"
    val ACTION_DELAY_TASK = "$PACKAGE_NAME.DELAYTASK"
}