package com.example.chores.util;

import androidx.databinding.InverseMethod;
import com.example.chores.data.model.NotificationUnit;
import com.example.chores.data.model.TaskPriority;
import com.example.chores.data.model.TaskStatus;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DataBindingConverter {

    @InverseMethod("intToPriority")
    public static Integer priorityToInt(TaskPriority value) {
        if (value != null)
            return value.ordinal();
        else
            return 0;
    }

    public static String priorityToString(TaskPriority value) {
        if (value != null)
            return value.toString();
        else
            return "";
    }

    public static String statusToString(TaskStatus value) {
        if (value != null)
            return value.toString();
        else
            return "";
    }

    public static TaskPriority intToPriority(Integer value) {
        return TaskPriority.values()[value];

    }

    public static String fromCalendarToDateString(Calendar value) {
        return  value != null ? new SimpleDateFormat("dd/MM/yyyy").format(value.getTimeInMillis()) : "";
    }

    public static String fromCalendarToTimeString(Calendar value) {
        return  value != null ? new SimpleDateFormat("HH:mm").format(value.getTimeInMillis()) : "";
    }

    @InverseMethod("stringToInt")
    public static String intToString(Integer value) {
        if (value >= 0)
            return value.toString();
        else
            return "";
    }

    public static Integer stringToInt(String value) {
        try
        {
            return Integer.parseInt(value);

        }
        catch (Exception e)
        {
            return -1;
        }
    }

    @InverseMethod("intToNotificationUnit")
    public static Integer notificationUnitToInt(NotificationUnit value) {
        return value.ordinal();
    }

    public static NotificationUnit intToNotificationUnit(Integer value) {
        return NotificationUnit.values()[value];
    }
}
