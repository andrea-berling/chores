package com.example.chores.util

import java.sql.Time
import java.util.*

object Utils {

    fun randomString(length: Int, characters: String, rng: Random):String {
        val text = CharArray(length)
        for (i in 0 until length)
            text[i] = characters[rng.nextInt(characters.length)]
        return String(text)
    }

    fun randomIdHelper(usedIds : List<String>) : String {
        val rng = Random(Time(0).time)
        var newId = randomString(24, "0123456789abcdef", rng)
        while (usedIds.contains(newId))
            newId = randomString(24, "0123456789abcdef", rng)
        return newId
    }

}