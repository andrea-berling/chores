package com.example.chores

import android.util.Log
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.*
import com.example.chores.data.model.NotificationUnit
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.util.Constants
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers
import org.hamcrest.Matchers.hasToString
import org.junit.Test
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.text.SimpleDateFormat
import java.util.*


@RunWith(AndroidJUnit4::class)
@LargeTest
class NotificationsTest: KodeinAware {

    override val kodein: Kodein by kodein(MainApplication.applicationContext())

    private val taskRepository : TaskRepository by instance()

    private val exampleTask = Task("0000000000000000000000000",
        "taskName",
        "taskDescription",
        TaskPriority.NEUTRAL,
        Constants.DEFAULT_CLASS_ID,
        Calendar.getInstance(),
        15,
        NotificationUnit.MINUTES,
        TaskStatus.ONGOING
    )

    @Test
    fun testTaskNotification() {

        runBlocking {

            exampleTask.taskClass = taskRepository.retrieveClass(exampleTask.taskClassId)
            taskRepository.addTask(exampleTask)

            val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
            device.openNotification()
            val timeout = 10000L
            assertTrue(device.wait(Until.hasObject(By.text(exampleTask.name)), timeout))
            val title = device.findObject(By.text(exampleTask.name))
            assertNotNull(title)
            val description = "The task \"${exampleTask.name}\" is due"
            val text = device.findObject(By.text(description))
            assertNotNull(text)
            assertEquals(exampleTask.name, title.text)
            assertEquals(description, text.text)

            title.clickAndWait(Until.newWindow(),3000L)

            device.openNotification()
            assertTrue(device.wait(Until.gone(By.text(exampleTask.name)), timeout))
        }
    }

    @Test
    fun testMarkOngoing() {

        runBlocking {

            exampleTask.taskClass = taskRepository.retrieveClass(exampleTask.taskClassId)
            taskRepository.addTask(exampleTask)

            val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
            device.openNotification()
            val timeout = 10000L
            assertTrue(device.wait(Until.hasObject(By.text(exampleTask.name)), timeout))
            onView(withId(android.R.id.button1))
            val markOngoing = device.findObject(By.text(MainApplication.applicationContext().getString(R.string.mark_ongoing).toUpperCase()))
            assertNotNull(markOngoing)

            markOngoing.click()

            device.openNotification()
            assertTrue(device.wait(Until.gone(By.text(exampleTask.name)), timeout))

            val task = taskRepository.retrieveTaskWithEverything(exampleTask.id)
            assertTrue(task?.let {
                it.status == TaskStatus.ONGOING
            } ?: false)
        }
    }

    @Test
    fun testDelayTask() {

        runBlocking {

            exampleTask.taskClass = taskRepository.retrieveClass(exampleTask.taskClassId)
            taskRepository.addTask(exampleTask)

            val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
            device.openNotification()
            val timeout = 10000L
            assertTrue(device.wait(Until.hasObject(By.text(exampleTask.name)), timeout))
            onView(withId(android.R.id.button1))
            val postponeTask = device.findObject(By.text(MainApplication.applicationContext().getString(R.string.postpone_task).toUpperCase()))
            assertNotNull(postponeTask)

            postponeTask.click()

            device.openNotification()
            assertTrue(device.wait(Until.gone(By.text(exampleTask.name)), timeout))
            device.pressBack()

            val newDateTime = Calendar.getInstance().apply {
                timeInMillis = exampleTask.datetime.timeInMillis
                add(Calendar.DAY_OF_YEAR,1)
                add(Calendar.HOUR_OF_DAY,5)
            }

            val year = newDateTime.get(Calendar.YEAR)
            val month = newDateTime.get(Calendar.MONTH)
            val day = newDateTime.get(Calendar.DAY_OF_MONTH)
            val hour = newDateTime.get(Calendar.HOUR_OF_DAY)
            val minute = newDateTime.get(Calendar.MINUTE)

            Log.d("DELAY_TASK","Date: ${SimpleDateFormat("dd/MM/yyyy : HH:mm").format(newDateTime.timeInMillis)}")

            onView(withId(R.id.pick_date)).perform(click())
            onView(ViewMatchers.withClassName(Matchers.equalTo(DatePicker::class.java.name))).perform(
                PickerActions.setDate(
                    year,
                    month + 1,
                    day
                )
            )
            onView(withId(android.R.id.button1)).perform(click())

            onView(withId(R.id.pick_time)).perform(click())
            onView(ViewMatchers.withClassName(Matchers.equalTo(TimePicker::class.java.name))).perform(
                PickerActions.setTime(
                    hour,
                    minute
                )
            )
            onView(withId(android.R.id.button1)).perform(click())

            onView(withId(R.id.notificationTimeText)).perform(clearText(),typeText("2"))

            onView(withId(R.id.notificationUnitSpinner)).perform(click())

            onData(hasToString("Hours")).perform(click())

            onView(withId(R.id.set_time_button)).perform(click())

            val task = taskRepository.retrieveTaskWithEverything(exampleTask.id)
            assertNotNull(task)
            assertTrue(task!!.datetime.get(Calendar.YEAR) == year)
            assertTrue(task.datetime.get(Calendar.MONTH) == month)
            assertTrue(task.datetime.get(Calendar.DAY_OF_MONTH) == day)
            assertTrue(task.datetime.get(Calendar.HOUR_OF_DAY) == hour)
            assertTrue(task.datetime.get(Calendar.MINUTE) == minute)
            assertTrue(task.notificationTime == 2)
            assertTrue(task.notificationUnit == NotificationUnit.HOURS)
        }
    }

}