package com.example.chores

import android.os.Bundle
import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.example.chores.data.database.AppDatabase
import com.example.chores.data.database.ClassDao
import com.example.chores.data.database.DependencyDao
import com.example.chores.data.database.TaskDao
import com.example.chores.data.model.*
import com.example.chores.data.repository.TaskRepository
import com.example.chores.util.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.hamcrest.CoreMatchers.equalTo
import org.junit.*
import org.junit.Assert.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.IOException
import java.util.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
/*
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.chores", appContext.packageName)
    }
}
 */

class DatabaseAndModelTest: KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val db : AppDatabase by instance()

    private val taskDao: TaskDao = db.taskDao()
    private val classDao: ClassDao = db.classDao()
    private val dependencyDao: DependencyDao = db.dependencyDao()

    @Test
    @Throws(Exception::class)
    fun insertTask() {
        val id = "000000000000000000000000"
        runBlocking {
            val task = Task(
                id,
                "Test name",
                "Test description",
                TaskPriority.NEUTRAL,
                Constants.DEFAULT_CLASS_ID,
                Calendar.getInstance(),
                50,
                NotificationUnit.MINUTES,
                TaskStatus.PENDING
            )

            taskDao.insert(task)

            val task2 = taskDao.getTaskWithId(id)

            assertThat(task, equalTo(task2))

            taskDao.delete(task)

        }
    }

    @Test
    @Throws(Exception::class)
    fun insertClass() {
        val id = "100000000000000000000000"
        runBlocking {
            val taskClass = TaskClass(
                id,
                "Test name",
                TaskPriority.NEUTRAL
            )

            classDao.insert(taskClass)

            val taskClass2 = classDao.getClass(id)

            assertThat(taskClass, equalTo(taskClass2))

            classDao.delete(taskClass)
        }
    }

    @Test
    @Throws(Exception::class)
    fun insertDependency() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        runBlocking {
            val task0 = Task(
                id0,
                "Test name",
                "Test description",
                TaskPriority.NEUTRAL,
                Constants.DEFAULT_CLASS_ID,
                Calendar.getInstance(),
                50,
                NotificationUnit.MINUTES,
                TaskStatus.PENDING
            )
            val task1 = Task(
                id1,
                "Test name",
                "Test description",
                TaskPriority.NEUTRAL,
                Constants.DEFAULT_CLASS_ID,
                Calendar.getInstance(),
                50,
                NotificationUnit.MINUTES,
                TaskStatus.PENDING
            )

            taskDao.insert(task0,task1)
            val dependency = TaskDependency(task0.id,task1.id)
            dependencyDao.insert(dependency)

            val depList = dependencyDao.getDependencies(id0)
            val dependency2 = depList[0]

            assertThat(dependency2, equalTo(dependency))
            assert(depList.size == 1)

            taskDao.delete(task0,task1)
        }
    }

    @Test
    fun testAfterTasks() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        runBlocking {
            val task0 = Task(
                id0,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task1 = Task(
                id1,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task2 = Task(
                id2,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task3 = Task(
                id3,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )

            task0.afterTasks = TaskList(listOf(task1,task2))
            task2.afterTasks = TaskList(listOf(task3))

            taskDao.insert(task0,task1,task2,task3)
            dependencyDao.insert(TaskDependency(task0.id,task1.id))
            dependencyDao.insert(TaskDependency(task0.id,task2.id))
            dependencyDao.insert(TaskDependency(task2.id,task3.id))

            val dep1 = taskDao.getAfterTasks(task0.id)
            val dep2 = taskDao.getAfterTasks(task2.id)
            dep1[1].afterTasks = TaskList(listOf(dep2[0]))

            assertTrue("Error on dep1",dep1.size == 2 && dep1.contains(task1) && dep1.contains(task2))
            assertTrue("Error on dep2",dep2.size == 1 && dep2.contains(task3))

            taskDao.delete(task0,task1,task2,task3)
        }
    }

    @Test
    fun deleteClassCascade() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        runBlocking {
            val task0 = Task(
                id0,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task1 = Task(
                id1,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task2 = Task(
                id2,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task3 = Task(
                id3,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )

            taskDao.insert(task0,task1,task2,task3)

            assertTrue("Error with task count",taskDao.getNumberOfTasksWithClass(Constants.DEFAULT_CLASS_ID) == 4)
            val defaultClass = classDao.getDefaultClass()
            classDao.delete(defaultClass)
            assertTrue("Error with class elimination",taskDao.getNumberOfTasksWithClass(Constants.DEFAULT_CLASS_ID) == 0)
            classDao.insert(defaultClass)

            taskDao.delete(task0,task1,task2,task3)
        }
    }

    @Test
    fun taskParcelization() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        val task0 = Task(
            id0,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task1 = Task(
            id1,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task2 = Task(
            id2,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task3 = Task(
            id3,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )

        task0.afterTasks = TaskList(listOf(task1,task2))
        task2.afterTasks = TaskList(listOf(task3))

        val bundle = Bundle()
        bundle.putParcelable("task",task0)

        val task0clone = bundle.getParcelable<Task>("task")

        Log.d("PARCEL_DEBUG","task0: $task0")
        Log.d("PARCEL_DEBUG","task0clone: $task0clone")

        assertThat(task0, equalTo(task0clone))

        // Fuck Android
    }

    @Test
    fun classParcelization() {

        val defaultClass = TaskClass(Constants.DEFAULT_CLASS_ID,"Default",TaskPriority.NEUTRAL)

        val bundle = Bundle()
        bundle.putParcelable("class",defaultClass)

        val defaultClassClone = bundle.getParcelable<TaskClass>("class")

        assertThat(defaultClass, equalTo(defaultClassClone))

        // Fuck Android
    }

    @Test
    fun dependencyCascadeElimination(){
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        val task0 = Task(
            id0,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task1 = Task(
            id1,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task2 = Task(
            id2,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task3 = Task(
            id3,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )

        task0.afterTasks = TaskList(listOf(task1,task2))
        task2.afterTasks = TaskList(listOf(task3))

        runBlocking {
            taskDao.insert(task0,task1,task2,task3)
            dependencyDao.insert(TaskDependency(task0.id,task1.id))
            dependencyDao.insert(TaskDependency(task0.id,task2.id))
            dependencyDao.insert(TaskDependency(task2.id,task3.id))

            taskDao.delete(task0)

            assertTrue("Problem on direct dependencies",dependencyDao.getDependencies(task0.id).isEmpty())
            assertTrue("Problem on inverse dependencies",dependencyDao.getInverseDependencies(task0.id).isEmpty())

            taskDao.delete(task1,task2,task3)
        }
    }

    @Test
    fun taskListTests() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        val task0 = Task(
            id0,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task1 = Task(
            id1,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task2 = Task(
            id2,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )
        val task3 = Task(
            id3,
            "Test name",
            Constants.DEFAULT_CLASS_ID
        )

        val taskList = TaskList().of(task0,task1,task2,task3)

        val bundle = Bundle()
        bundle.putParcelable("list",taskList)

        val taskListClone = bundle.getParcelable<TaskList>("list")

        assertThat(taskList, equalTo(taskListClone))

        assertTrue(taskList.indexOfId(id0) == 0)
        assertTrue(taskList.indexOfId(id1) == 1)
        assertTrue(taskList.indexOfId(id2) == 2)
        assertTrue(taskList.indexOfId(id3) == 3)
        assertTrue(taskList.indexOfId("0$id0") == -1)

        val firstTwo = taskList.filter {
            return@filter it.id <= id1
        }

        assertTrue(firstTwo.size == 2)
        assertTrue(firstTwo.contains(task0))
        assertTrue(firstTwo.contains(task1))

        val empty = taskList.filter {
            false
        }

        val clone = taskList.filter {
            true
        }

        assertTrue(empty.isEmpty())
        assertThat(taskList, equalTo(clone))
    }
}

class TaskRepositoryTests: KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val taskRepository : TaskRepository by instance()

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    @Test
    fun addTaskTest() {
        val id0 = "000000000000000000000000"
        val id1 = "000000000000000000000001"
        val id2 = "000000000000000000000002"
        val id3 = "000000000000000000000003"
        runBlocking {
            val task0 = Task(
                id0,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task1 = Task(
                id1,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task2 = Task(
                id2,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )
            val task3 = Task(
                id3,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )

            val defaultClass = TaskClass(Constants.DEFAULT_CLASS_ID,"Default",TaskPriority.NEUTRAL)

            task0.afterTasks = TaskList(listOf(task1, task2))
            task0.taskClass = defaultClass
            task2.afterTasks = TaskList(listOf(task3))
            task2.taskClass = defaultClass
            task1.taskClass = defaultClass
            task3.taskClass = defaultClass

            taskRepository.addTask(task3)
            taskRepository.addTask(task1)
            taskRepository.addTask(task2)
            taskRepository.addTask(task0)

            val task0fromRepo = taskRepository.retrieveTaskWithEverything(id0)
            val task2fromRepo = taskRepository.retrieveTaskWithEverything(id2)

            assertTrue(task0.similarTo(task0fromRepo!!))
            task0fromRepo.afterTasks!!.run {
                assertTrue(get(indexOfId(id1)).similarTo(task1))
                assertTrue(get(indexOfId(id2)).similarTo(task2))
            }
            assertThat(task0.taskClass, equalTo(task0fromRepo.taskClass))

            assertTrue(task2.similarTo(task2fromRepo!!))
            task2fromRepo.afterTasks!!.run {
                assertTrue(get(indexOfId(id3)).similarTo(task3))
            }
            assertThat(task2.taskClass, equalTo(task2fromRepo.taskClass))

            taskRepository.removeTask(id0)
            taskRepository.removeTask(id1)
            taskRepository.removeTask(id2)
            taskRepository.removeTask(id3)
        }
    }

    @Test
    fun updateTaskTest() {
        val id0 = "000000000000000000000000"
        runBlocking {
            val task0 = Task(
                id0,
                "Test name",
                Constants.DEFAULT_CLASS_ID
            )

            val defaultClass = TaskClass(Constants.DEFAULT_CLASS_ID,"Default",TaskPriority.NEUTRAL)

            task0.taskClass = defaultClass
            task0.afterTasks = TaskList()

            taskRepository.addTask(task0)

            val task0clone = Task(task0)
            task0.status = TaskStatus.COMPLETED
            taskRepository.updateTask(task0)

            val task0fromRepo = taskRepository.retrieveTaskWithEverything(id0)

            assertNotEquals(task0clone,task0fromRepo)
            assertTrue(task0.status == TaskStatus.COMPLETED)

            task0fromRepo!!.status = TaskStatus.PENDING
            assertThat(task0fromRepo, equalTo(task0clone))

            taskRepository.removeTask(id0)
        }
    }

    @Test
    fun futureTasksTest() {
    }
}

private fun Task.similarTo(task2: Task) : Boolean {
    return (this.id == task2.id) &&
    (this.name == task2.name) &&
    (this.description == task2.description) &&
    (this.priority == task2.priority) &&
    (this.taskClassId == task2.taskClassId) &&
    (this.datetime == task2.datetime) &&
    (this.notificationTime == task2.notificationTime) &&
    (this.notificationUnit == task2.notificationUnit) &&
    (this.status == task2.status)
}
