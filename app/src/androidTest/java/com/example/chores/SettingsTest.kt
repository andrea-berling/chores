package com.example.chores

import android.util.Log
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.main.MainActivity
import com.example.chores.ui.main.MainActivityViewModel
import com.example.chores.ui.main.MainActivityViewModelFactory
import com.example.chores.ui.notifications.NotificationCenter
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matchers.hasToString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

@RunWith(AndroidJUnit4::class)
@LargeTest
class SettingsTest: KodeinAware {

    override val kodein: Kodein by kodein(MainApplication.applicationContext())

    private val mainActivityViewModelFactory : MainActivityViewModelFactory by instance()
    private val taskRepository : TaskRepository by instance()
    private val notificationCenter : NotificationCenter by instance()

    @get:Rule
    val activityRule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun taskPriorityNotificationFilters(){

        runBlocking {
            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

            onView(withText("Settings")).perform(click())

            onView(withId(R.id.recycler_view)).run {
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
                /* Android Preference matchers don't work on Android X. F**k Android */
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
            }

            //onData(withKey("notification_priority_filter")).perform(click())

            onData(hasToString("Low")).perform(click())
            onData(hasToString("High")).perform(click())

            onView(withId(android.R.id.button1)).perform(click())

            pressBack()

            Thread.sleep(1000)

            val tasks = taskRepository.retrieveFutureTasks(null)

            tasks.forEach {
                Log.d("SETTINGS_DEBUG","task: ${it.name}, priority: ${it.priority}, notification set: ${notificationCenter.isNotificationSet(it)}")
                when(it.priority) {
                    TaskPriority.LOW,TaskPriority.HIGH -> assertTrue(notificationCenter.isNotificationSet(it))
                    TaskPriority.NEUTRAL,TaskPriority.VERY_HIGH -> assertFalse(notificationCenter.isNotificationSet(it))
                }
            }

        }
    }

    @Test
    fun taskClassPriorityNotificationFilter() {
        runBlocking {
            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

            onView(withText("Settings")).perform(click())

            onView(withId(R.id.recycler_view)).run {
                /* Android Preference matchers don't work on Android X. F**k Android */
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(4, click()))
            }

            onData(hasToString("Low")).perform(click())
            onData(hasToString("High")).perform(click())

            onView(withId(android.R.id.button1)).perform(click())

            pressBack()

            Thread.sleep(1000)

            val tasks = taskRepository.retrieveFutureTasks(null)

            tasks.forEach {
                //Log.d("SETTINGS_DEBUG","task: ${it.name}, priority: ${it.priority}, notification set: ${notificationCenter.isNotificationSet(it)}")
                val taskClass = taskRepository.retrieveClass(it.taskClassId)
                assertNotNull(taskClass)
                when (taskClass!!.priority) {
                    TaskPriority.LOW, TaskPriority.HIGH -> assertTrue(
                        notificationCenter.isNotificationSet(
                            it
                        )
                    )
                    TaskPriority.NEUTRAL, TaskPriority.VERY_HIGH -> assertFalse(
                        notificationCenter.isNotificationSet(
                            it
                        )
                    )
                }
            }

        }
    }

    @Test
    fun taskClassNotificationFilter(){

        runBlocking {
            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

            onView(withText("Settings")).perform(click())

            onView(withId(R.id.recycler_view)).run {
                /* Android Preference matchers don't work on Android X. F**k Android */
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(5, click()))
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(6, click()))
            }

            onData(hasToString("Class02")).perform(click())
            onData(hasToString("Class03")).perform(click())

            onView(withId(android.R.id.button1)).perform(click())

            pressBack()

            Thread.sleep(1000)

            val tasks = taskRepository.retrieveFutureTasks(null)

            tasks.forEach {
                //Log.d("SETTINGS_DEBUG","task: ${it.name}, priority: ${it.priority}, notification set: ${notificationCenter.isNotificationSet(it)}")
                when(it.taskClassId) {
                    "02","03" -> assertTrue(notificationCenter.isNotificationSet(it))
                    else -> assertFalse(notificationCenter.isNotificationSet(it))
                }
            }

        }
    }

    @Test
    fun taskTodoFilter(){

        runBlocking {
            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

            onView(withText("Settings")).perform(click())

            onView(withId(R.id.recycler_view)).run {
                /* Android Preference matchers don't work on Android X. F**k Android */
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(8, click()))
            }

            pressBack()

            Thread.sleep(1000)

            val viewModel = ViewModelProviders.of(activityRule.activity,mainActivityViewModelFactory)[MainActivityViewModel::class.java]

            val tasks = viewModel.tasks

            tasks.forEach {
                //Log.d("SETTINGS_DEBUG","task: ${it.name}, priority: ${it.priority}, notification set: ${notificationCenter.isNotificationSet(it)}")
                if(it.status == TaskStatus.COMPLETED)
                    assertTrue(false)
            }

            assertTrue(true)
        }
    }

    @Test
    fun taskPriorityFilter(){

        runBlocking {
            openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)

            onView(withText("Settings")).perform(click())

            onView(withId(R.id.recycler_view)).run {
                /* Android Preference matchers don't work on Android X. F**k Android */
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(9, click()))
                perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(10, click()))
            }

            onData(hasToString("Low")).perform(click())
            onData(hasToString("High")).perform(click())

            onView(withId(android.R.id.button1)).perform(click())

            pressBack()

            Thread.sleep(1000)

            val viewModel = ViewModelProviders.of(activityRule.activity,mainActivityViewModelFactory)[MainActivityViewModel::class.java]

            val tasks = viewModel.tasks

            tasks.forEach {
                //Log.d("SETTINGS_DEBUG","task: ${it.name}, priority: ${it.priority}, notification set: ${notificationCenter.isNotificationSet(it)}")
                if(it.priority == TaskPriority.NEUTRAL || it.priority == TaskPriority.VERY_HIGH )
                    assertTrue(false)
            }

            assertTrue(true)
        }
    }
}