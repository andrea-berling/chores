package com.example.chores

import android.content.Intent
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.lifecycle.ViewModelProviders
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.contrib.PickerActions
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.example.chores.data.model.NotificationUnit
import com.example.chores.data.model.Task
import com.example.chores.data.model.TaskPriority
import com.example.chores.data.model.TaskStatus
import com.example.chores.data.repository.TaskRepository
import com.example.chores.ui.edit_task.EditTaskActivity
import com.example.chores.ui.edit_task.EditTaskViewModel
import com.example.chores.ui.edit_task.EditTaskViewModelFactory
import com.example.chores.util.Constants
import junit.framework.Assert.assertTrue
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.hasToString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*


@RunWith(AndroidJUnit4::class)
@LargeTest
class EditTaskActivityTest:KodeinAware {

    override val kodein by kodein(MainApplication.applicationContext())
    private val viewModelFactory : EditTaskViewModelFactory by instance()

    @get:Rule
    // lazy launch
    var activityRule: ActivityTestRule<EditTaskActivity> = ActivityTestRule(EditTaskActivity::class.java,true,false)
    //var activityRule: ActivityTestRule<EditTaskActivity> = ActivityTestRule(EditTaskActivity::class.java)

    private val exampleTask = Task("0",
        "taskName",
        "taskDescription",
        TaskPriority.NEUTRAL,
        Constants.DEFAULT_CLASS_ID,
        Calendar.getInstance().apply {
            add(Calendar.HOUR_OF_DAY,1)
        },
        15,
        NotificationUnit.MINUTES,
        TaskStatus.ONGOING
        )

    @Test
    fun bidirectionalBinding() {

        activityRule.launchActivity(null)

        val viewModel = ViewModelProviders.of(activityRule.activity,viewModelFactory)[EditTaskViewModel::class.java]

        compileTask()

        verifyTaskAgainstViewModel(exampleTask,viewModel)

    }

    private fun verifyTaskAgainstViewModel(task: Task, viewModel: EditTaskViewModel) {

        val year = task.datetime.get(Calendar.YEAR)
        val month = task.datetime.get(Calendar.MONTH)
        val day = task.datetime.get(Calendar.DAY_OF_MONTH)
        val hour = task.datetime.get(Calendar.HOUR_OF_DAY)
        val minute = task.datetime.get(Calendar.MINUTE)

        assertTrue(viewModel.name.value?.let {
            it == task.name
        } ?: false)

        assertTrue(viewModel.description.value?.let {
            it == task.description
        } ?: false)

        assertTrue(viewModel.priority.value?.let {
            it == task.priority
        } ?: false)

        assertTrue(viewModel.classIndex.value?.let {
            viewModel.classes[it].id == Constants.DEFAULT_CLASS_ID
        } ?: false)

        assertTrue(viewModel.date.value?.let {
            it.get(Calendar.YEAR) == year &&
                    it.get(Calendar.MONTH) == month &&
                    it.get(Calendar.DAY_OF_MONTH) == day &&
                    it.get(Calendar.HOUR_OF_DAY) == hour &&
                    it.get(Calendar.MINUTE) == minute
        } ?: false)

        assertTrue(viewModel.notificationTime.value?.let {
            it == task.notificationTime
        } ?: false)

        assertTrue(viewModel.notificationUnit.value?.let {
            it == task.notificationUnit
        } ?: false)
    }

    @Test
    fun createTaskCorrectIntent(){

        activityRule.launchActivity(null)

        compileTask()

        onView(withId(R.id.confirm_button)).perform(scrollTo(),click())

        val intent = activityRule.activityResult.resultData

        assertThat(intent, hasExtraWithKey("result"))

        val task : Task? = intent.extras?.getParcelable("result")

        task?.run {
            assertTrue(name == exampleTask.name)
            assertTrue(description == exampleTask.description)
            assertTrue(priority == exampleTask.priority)
            assertTrue(taskClassId == Constants.DEFAULT_CLASS_ID)
            assertTrue(sameDate(datetime,exampleTask.datetime))
            assertTrue(notificationTime == exampleTask.notificationTime)
            assertTrue(notificationUnit == exampleTask.notificationUnit)
            assertTrue(status == TaskStatus.PENDING)
        } ?: assertTrue(false)
    }

    /* This test needs lazy activity launch */
    @Test
    fun editTaskCorrectLoading(){


        val intent = Intent(MainApplication.applicationContext(),EditTaskActivity::class.java).apply {
            putExtra("task",exampleTask)
        }

        activityRule.launchActivity(intent)

        val viewModel = ViewModelProviders.of(activityRule.activity,viewModelFactory)[EditTaskViewModel::class.java]

        assertTrue(viewModel.id == exampleTask.id)

        verifyTaskAgainstViewModel(exampleTask,viewModel)

        assertTrue(viewModel.status == exampleTask.status)
    }


    /* This test needs lazy activity launch */
    @Test
    fun editTaskCorrectIntent(){

        val intent = Intent(MainApplication.applicationContext(),EditTaskActivity::class.java).apply {
            putExtra("task",exampleTask)
        }

        activityRule.launchActivity(intent)

        val newName = "taskNewName"

        val newDateTime = Calendar.getInstance().apply {
            timeInMillis = exampleTask.datetime.timeInMillis
            add(Calendar.DAY_OF_MONTH,5)
        }

        val year = newDateTime.get(Calendar.YEAR)
        val month = newDateTime.get(Calendar.MONTH)
        val day = newDateTime.get(Calendar.DAY_OF_MONTH)

        onView(withId(R.id.taskName)).perform(clearText(),typeText(newName))

        onView(withId(R.id.pick_date)).perform(click())
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(
            PickerActions.setDate(
                year,
                month + 1,
                day
            )
        )
        onView(withId(android.R.id.button1)).perform(click())

        onView(withId(R.id.confirm_button)).perform(scrollTo(), click())

        val newTask : Task? = activityRule.activityResult.resultData.getParcelableExtra("result")

        newTask?.run {
            assertTrue(newTask.id == exampleTask.id)
            assertTrue(newTask.name == newName)
            assertTrue(newTask.description == exampleTask.description)
            assertTrue(newTask.priority == exampleTask.priority)
            assertTrue(newTask.taskClassId == exampleTask.taskClassId)
            assertTrue(sameDate(newTask.datetime,newDateTime))
            assertTrue(newTask.notificationTime == exampleTask.notificationTime)
            assertTrue(newTask.notificationUnit == exampleTask.notificationUnit)
            assertTrue(newTask.status == exampleTask.status)
        } ?: assertTrue(false)
    }

    private fun sameDate(datetime: Calendar, datetime1: Calendar): Boolean {
        return datetime.get(Calendar.YEAR) == datetime1.get(Calendar.YEAR) &&
                datetime.get(Calendar.MONTH) == datetime1.get(Calendar.MONTH) &&
                datetime.get(Calendar.DAY_OF_MONTH) == datetime1.get(Calendar.DAY_OF_MONTH) &&
                datetime.get(Calendar.HOUR_OF_DAY) == datetime1.get(Calendar.HOUR_OF_DAY) &&
                datetime.get(Calendar.MINUTE) == datetime1.get(Calendar.MINUTE)
    }

    private fun compileTask() {
        onView(withId(R.id.taskName)).perform(typeText(exampleTask.name))
        onView(withId(R.id.taskDescription)).perform(typeText(exampleTask.description))

        onView(withId(R.id.priority_spinner)).perform(click())
        onData(hasToString(exampleTask.priority.toString())).perform(click())

        onView(withId(R.id.class_spinner)).perform(click())
        onData(hasToString("Default")).perform(click())

        val year = exampleTask.datetime.get(Calendar.YEAR)
        val month = exampleTask.datetime.get(Calendar.MONTH)
        val day = exampleTask.datetime.get(Calendar.DAY_OF_MONTH)
        val hour = exampleTask.datetime.get(Calendar.HOUR_OF_DAY)
        val minute = exampleTask.datetime.get(Calendar.MINUTE)

        onView(withId(R.id.pick_date)).perform(click())
        onView(withClassName(equalTo(DatePicker::class.java.name))).perform(
            PickerActions.setDate(
                year,
                month + 1,
                day
            )
        )
        onView(withId(android.R.id.button1)).perform(click())

        onView(withId(R.id.pick_time)).perform(click())
        onView(withClassName(equalTo(TimePicker::class.java.name))).perform(
            PickerActions.setTime(
                hour,
                minute
            )
        )
        onView(withId(android.R.id.button1)).perform(click())

        onView(withId(R.id.notification_time)).perform(clearText())
            .perform(typeText(exampleTask.notificationTime.toString()))

        onView(withId(R.id.notification_unit_spinner)).perform(click())
        onData(hasToString(exampleTask.notificationUnit.toString().toLowerCase().capitalize())).perform(
            click()
        )
    }
}

